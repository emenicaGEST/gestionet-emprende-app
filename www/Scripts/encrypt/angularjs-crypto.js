"use strict"
function missingCryptoJs(e,t,n){return e?t.key().length<=0?!1:"undefined"==typeof CryptoJS?!0:void 0:!1}function decrypt(e,t,n){void 0!==e&&null!==e?(log("------ decrypt key "+t.key()),crypt(e,n,t.plugin.decode,t.key())):log("data for decryption was null!")}function encrypt(e,t,n){void 0!==e&&null!==e?(log("------ crypt key "+t.key()),crypt(e,n,t.plugin.encode,t.key())):log("data for encryption was null!")}function crypt(e,t,n,r){if("undefined"!==e&&null!==e){var o=Object.keys(e)
for(var i in o)void 0!==t&&null!==e[o[i]]&&"undefined"!==e[o[i]]&&o[i].endsWith(t)&&(e[o[i]]=n(e[o[i]],r)),"object"==typeof e[o[i]]&&crypt(e[o[i]],t,n,r),(void 0===t||"*"===t)&&(e[o[i]]=n(e[o[i]],r))}}function checkHeader(e,t){return t?e.contentHeaderCheck.check(t):!1}function defaultVal(e,t){return void 0===e?t:e}function log(e,t){e.logging&&console.log(t)}function ContentHeaderCheck(e){var e=e
return{check:function(t){for(var n=0;n<e.length;n++){var r=t.beginsWith(e[n])
if(r)return!0}return!1}}}var cryptoModule=angular.module("angularjs-crypto",["ngCookies"])
cryptoModule.config(["$httpProvider",function(e){var t=["$q","cfCryptoHttpInterceptor",function(e,t){return{request:function(n){var r=n.crypt||!1,o=n.pattern||t.pattern
if(missingCryptoJs(r,t,e))return q.reject("CryptoJS missing")
var i=n.data
if(r===!0)if(checkHeader(t,n.headers["Content-Type"])){if(log(t,"intercept request "+angular.toJson(i)),!i)return e.reject(n)
encrypt(i,t,o)}else void 0!==n.params&&encrypt(n.params,t,o)
else if(n.fullcryptbody){if(!i)return e.reject(n)
n.data=t.plugin.encode(JSON.stringify(i),t.key()),log(t,"encode full body "+n.data)}else void 0!==n.params&&(log(t,"encode full query "+n.params),n.params={query:t.plugin.encode(JSON.stringify(n.params),t.key())},log(t,"encode full query "+n.params))
return n},response:function(n){var r=(n.config||!1).crypt&&defaultVal(n.config.decrypt,!0),o=n.config&&n.config.pattern
if(o=o||t.pattern,missingCryptoJs(r,t,e))return q.reject("CryptoJS missing")
if(1==r){if(checkHeader(t,n.headers()["content-type"])){var i=n.data
if(log(t,"intercept response "+angular.toJson(i)),i.base,t.base64Key=i.base,!i)return e.reject(n)
decrypt(i,t,o)}}else if(n.config.decryptbody&&checkHeader(t,n.headers()["content-type"])){var i=n.data
if(!i)return e.reject(request)
n.data=JSON.parse(t.plugin.decode(i,t.key())),log(t,"encode full body "+n.data)}return n}}}]
e.interceptors.push(t)}]),cryptoModule.provider("cfCryptoHttpInterceptor",function(){this.base64Key,this.base64KeyFunc=function(){return""},this.pattern="_enc",this.logging=!1,this.plugin=new CryptoJSCipher(CryptoJS.mode.ECB,CryptoJS.pad.Pkcs7,CryptoJS.TripleDES),this.contentHeaderCheck=new ContentHeaderCheck(["application/json","application/json_enc"]),this.responseWithQueryParams=!0,this.$get=function(){return{base64Key:this.base64Key,base64KeyFunc:this.base64KeyFunc,key:function(){return this.base64Key||this.base64KeyFunc()},pattern:this.pattern,plugin:this.plugin,contentHeaderCheck:this.contentHeaderCheck,responseWithQueryParams:this.responseWithQueryParams}}}),String.prototype.beginsWith=function(e){return 0===this.indexOf(e)},String.prototype.endsWith=function(e){var t=this.lastIndexOf(e)
return-1!=t&&t+e.length==this.length}
