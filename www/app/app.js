
var emprendeapp = angular.module('emprendeapp', ['ngRoute', 'ngTouch', 'ngCookies', 'ui.bootstrap', 'pascalprecht.translate','ng-currency','tmh.dynamicLocale', 'timer','angularjs-crypto']);


emprendeapp.constant("MONEDAS", {
        "EURO": 0,
        "DOLAR": 1,
        "LIBRA": 2,
		"PESO": 3
    });
	
/* LOCALSTORAGE ENCRYPT */
/*var ss = new cordova.plugins.SecureStorage(
	function () { console.log('Success')},
	function (error) { console.log('Error ' + error); },
	'emprendeapp');
*/
emprendeapp.config(['$routeProvider','$httpProvider','$locationProvider', '$translateProvider','tmhDynamicLocaleProvider', function ($routeProvider, $httpProvider, $locationProvider, $translateProvider, tmhDynamicLocaleProvider) {
	
	//cargar locale correspondiente
	tmhDynamicLocaleProvider.localeLocationPattern("app/locales/angular-locale_{{locale}}.js");
	//rutas
	$routeProvider
        .when('/', { 
			templateUrl: 'app/templates/bienvenida.html'
		})
        .when('/registro', { 
			templateUrl: 'app/templates/registro.html'	
		})
        .when('/prueba', { 
			templateUrl: 'app/templates/prueba.html'
		})
		.when('/bienvenida', {
			templateUrl: 'app/templates/bienvenida.html'
		})
		.when('/menuPrincipal', {
			templateUrl: 'app/templates/menuPrincipal.html'
		})
		.when('/nuevaPartida', {
			templateUrl: 'app/templates/nuevaPartida.html'
		})
		.when('/nuevaPartidaSingle', {
			templateUrl: 'app/templates/nuevaPartidaSingle.html'
		})
		.when('/nuevaPartidaMulti', {
			templateUrl: 'app/templates/nuevaPartidaMulti.html'
		})
		.when('/continuarPartida', {
			templateUrl: 'app/templates/continuarPartida.html'
		})
		/*.when('/continuarPartidaSingle', {
			templateUrl: 'app/templates/continuarPartidaSingle.html'
		})
		.when('/continuarPartidaMulti', {
			templateUrl: 'app/templates/continuarPartidaMulti.html'
		})*/
		.when('/opciones', {
			templateUrl: 'app/templates/opciones.html'
		})
		.when('/miEmpresa/:idEmpresa', {
			templateUrl: 'app/templates/miEmpresa.html'
		})
		.when('/miEmpresa', {
			templateUrl: 'app/templates/miEmpresa.html'
		})
		.when('/balance', {
			templateUrl: 'app/templates/balance.html'
		})
		.when('/pyg', {
			templateUrl: 'app/templates/pyg.html'
		})
		.when('/efe', {
			templateUrl: 'app/templates/efe.html'
		})
		.when('/inversiones', {
			templateUrl: 'app/templates/inversiones.html'
		})
		.when('/prestamos', {
			templateUrl: 'app/templates/prestamos.html'
		})
		.when('/subvenciones', {
			templateUrl: 'app/templates/subvenciones.html'
		})
		.when('/proveedores', {
			templateUrl: 'app/templates/proveedores.html'
		})
		.when('/publicidad', {
			templateUrl: 'app/templates/publicidad.html'
		})
		.when('/contratacion', {
			templateUrl: 'app/templates/contratacion.html'
		})
		.when('/organizacion', {
			templateUrl: 'app/templates/organizacion.html'
		})
		.when('/proyectos', {
			templateUrl: 'app/templates/proyectos.html'
		})
		.when('/presupuestos', {
			templateUrl: 'app/templates/presupuestos.html'
		})
		.when('/mercado', {
			templateUrl: 'app/templates/mercado.html'
		})
		.when('/necesidades', {
			templateUrl: 'app/templates/necesidades.html'
		})
		.when('/alertas', {
            templateUrl: 'app/templates/alertas.html'
        })
        .when('/ayudaConfigurarSingle', {
            templateUrl: 'app/templates/ayudaConfigurarSingle.html'
        })
		.when('/ayudaConfigurarMulti', {
            templateUrl: 'app/templates/ayudaConfigurarMulti.html'
        })
		.when('/infoUsuario', {
			templateUrl: 'app/templates/infoUsuario.html'
		})
		.when('/informacionGestion', {
			templateUrl: 'app/templates/informacionGestion.html'
		})
        .when('/informacionPublicidad', {
            templateUrl: 'app/templates/informacionPublicidad.html'
        })
        .when('/informacionIf', {
            templateUrl: 'app/templates/informacionIf.html'
        })
		.when('/informacion/:pantalla/:nombre/:parrafo_uno/:parrafo_dos', {
			templateUrl: 'app/templates/informacion.html'
		})
        .when('/rankingSeleccionarMultiSingle', {
        templateUrl: 'app/templates/rankingSeleccionarMultiSingle.html'
		})
		.when('/rankingMulti', {
        templateUrl: 'app/templates/rankingMulti.html'
		})
		.when('/ranking', {
        templateUrl: 'app/templates/ranking.html'
		})
		.when('/ayuda', {
			templateUrl: 'app/templates/ayuda.html'
		})
		.when('/mantenimiento', {
			templateUrl: 'app/templates/mantenimiento.html'
		})		
		.when('/pasoJornada', {
			templateUrl: 'app/templates/pasoJornada.html'
		})
		.when('/pasoJornada/:estado', {
			templateUrl: 'app/templates/pasoJornada.html'
		})
		.when('/concurso', {
			templateUrl: 'app/templates/concurso.html'
		})
		.when('/login', {
			templateUrl: 'app/templates/login.html'
		})
		.when('/comoParticipar', {
			templateUrl: 'app/templates/comoParticipar.html'
		})
		.when('/bases', {
			templateUrl: 'app/templates/bases.html'
		})
		.when('/calendario', {
			templateUrl: 'app/templates/calendario.html'
		})
		.when('/logros', {
			templateUrl: 'app/templates/logros.html'
		})
		.when('/rankingConcurso', {
			templateUrl: 'app/templates/rankingConcurso.html'
		})
		.when('/premios', {
			templateUrl: 'app/templates/premios.html'
		})
		.when('/concursoLigaNacional', {
			templateUrl: 'app/templates/concursoLigaNacional.html'
		})
		.when('/concursoFaseFinal', {
			templateUrl: 'app/templates/concursoFaseFinal.html'
		})
		.when('/recuperarPassword', {
			templateUrl: 'app/templates/recuperarPassword.html'
		})
        .otherwise({ redirectTo: '/' });

	var engTranslation = getEngTranslation();
	var espTranslation = getEspTranslation();
	var ptTranslation = getPtTranslation();
	var deTranslation = getDeTranslation();

	$translateProvider.translations('en', engTranslation);
	$translateProvider.translations('es', espTranslation);
	$translateProvider.translations('pt', ptTranslation);
	$translateProvider.translations('de', deTranslation);
	$translateProvider.preferredLanguage('es');
	var lang = navigator.language.split("-")[0];
	if(lang!= 'es' && lang!='en' && lang!='pt' && lang!='de'){
		lang = 'en';
	}
	
	if( window.localStorage.getItem("idiomaElegido") != null){
		$translateProvider.preferredLanguage( window.localStorage.getItem("idiomaElegido"));
	}
	else{

		$translateProvider.preferredLanguage(lang);
	}

	$httpProvider.interceptors.push('requestInterceptor');

}])


emprendeapp.factory('requestInterceptor', function ($location, $q, $rootScope) {
	$rootScope.pendingRequests = 0;
	//console.log( "requestInterceptor ")
	//console.log($location);
	return {
		'request': function (config) {
			if(config.url.match("https://")) {
				$rootScope.showOverlay = true;
				var dispositivo = navigator.userAgent.toLowerCase();
				if( dispositivo.search(/iphone|ipod|ipad|android/) > -1 ){
					$rootScope.checkConnection();
				}
			}
			$rootScope.pendingRequests++;
			return config || $q.when(config);


		},

		'requestError': function (rejection) {
			$rootScope.pendingRequests--;
			return $q.reject(rejection);
		},

		'response': function (response) {
			$rootScope.showOverlay = false;
			if(response.data.status) {				
				var dispositivo = navigator.userAgent.toLowerCase();
				if( dispositivo.search(/iphone|ipod|ipad|android/) > -1 ){
					$rootScope.checkConnection();
				}
			}

			$rootScope.pendingRequests--;
			if (response.data.status == "fail") {
				console.log(response.data.mensaje);
			}
			return response || $q.when(response);
		},

		'responseError': function (rejection) {
			$rootScope.showOverlay = false;
			if (rejection.status == 401) {
				$location.path('/');
			} else if (rejection.data.status == "fail") {
				alert(rejection.data.mensaje)
			}

			return $q.reject(rejection);
		}

	}
});
//función para quitar formateo español y convertir a formato int por defecto
emprendeapp.filter("unformat", function ()
{
   return function (inputNumber) {
	// Convertir a string
	formattedNumber = inputNumber.toString();
	// Extraer la parte decimal
	var numberParts = formattedNumber.split(",");
	// Eliminar el "." de los miles si se encuentra
	numberParts[0] = numberParts[0].replace(".", "");

	// Compose the final result
	var result = numberParts[0];
	if (numberParts.length == 2)
	{
	  result += "," + numberParts[1];
	}
	return result;
    };
});
//eliminar decimales al usar el filtro currency
emprendeapp.filter('mycurrency', [ '$filter', '$locale', function ($filter, $locale) {
	var currency = $filter('currency'), formats = $locale.NUMBER_FORMATS;
	return function (amount, symbol) {
		var value = currency(amount, symbol);
		return value.replace(
			new RegExp('\\' + formats.DECIMAL_SEP + '\\d{2}'), '');
	} 
}]);

emprendeapp.filter("customCurrency", function (numberFilter, $rootScope)
  {
    function isNumeric(value)
    {
      return (!isNaN(parseFloat(value)) && isFinite(value));
    }

    return function (inputNumber, currencySymbol, decimalSeparator, thousandsSeparator, decimalDigits, keepFormat) {
      if (isNumeric(inputNumber))
      {
        // Default values for the optional arguments
        currencySymbol = (typeof currencySymbol === "undefined") ? "$" : currencySymbol;
        decimalSeparator = (typeof decimalSeparator === "undefined") ? "." : decimalSeparator;
        thousandsSeparator = (typeof thousandsSeparator === "undefined") ? "," : thousandsSeparator;
        decimalDigits = (typeof decimalDigits === "undefined" || !isNumeric(decimalDigits)) ? 2 : decimalDigits;
		keepFormat = (typeof keepFormat === "undefined") ? false : keepFormat;
		
		if (keepFormat == false)
		{
			//adecuar formateo según moneda
			if ($rootScope.MonedaActual == $rootScope.MONEDAS.EURO)
			{
				decimalSeparator = ",";
				thousandsSeparator = ".";
			}
			else if ($rootScope.MonedaActual == $rootScope.MONEDAS.DOLAR || $rootScope.MonedaActual == $rootScope.MONEDAS.LIBRA)
			{
				decimalSeparator = ".";
				thousandsSeparator = ",";
			}
			else if ($rootScope.MonedaActual == $rootScope.MONEDAS.PESO)
			{
				decimalSeparator = ".";
				thousandsSeparator = "";
			}
			
			if (decimalDigits < 0) decimalDigits = 0;

			// Format the input number through the number filter
			// The resulting number will have "," as a thousands separator
			// and "." as a decimal separator.
			var formattedNumber = numberFilter(inputNumber, decimalDigits);

			// Extract the integral and the decimal parts
			var numberParts = formattedNumber.split(".");

			// Replace the "," symbol in the integral part
			// with the specified thousands separator.
			numberParts[0] = numberParts[0].split(",").join(thousandsSeparator);

			// Compose the final result
			var result = numberParts[0];

			if (numberParts.length == 2)
			{
			  result += decimalSeparator + numberParts[1];
			}
		}
		else
		{
			result = inputNumber;
		}
		if ($rootScope.MonedaActual == $rootScope.MONEDAS.DOLAR || $rootScope.MonedaActual == $rootScope.MONEDAS.LIBRA)
		{
			result = currencySymbol + " " + result;
		} 
		else
		{
			result += " " + currencySymbol;
		}
        return result;
      }
      else
      {
        return inputNumber;
      }
    };
});


emprendeapp.run(function($rootScope, $location, $translate, MONEDAS, tmhDynamicLocale, $locale, dataService, storageService, cfCryptoHttpInterceptor) {
	
	//cfCryptoHttpInterceptor.base64Key = "1234567890123456";
	//cfCryptoHttpInterceptor.pattern = "_enc";//"*";
	//cfCryptoHttpInterceptor.logging = true;	
	

/*
	var parameters = [];
	var param = {};
	var processedParams = {};
	
	
	//test
	param['data'] = "prueb_data texto largo de cojones 122@ con acentós bla bla";
	param['name'] = "nickname";
	parameters.push(param);
	param = {};
	param['data'] = "arrobarroba@midominio.co.mx";
	param['name'] = "email";		
	parameters.push(param);
	processedParams = dataService.splitEncryptedParamData(parameters);
	
	var dataparam = angular.toJson(processedParams);
	console.log("dataparam " + dataparam);
*/

	//console.log("app run"); 
	
	/* file access */
	/*
	var errorHandler = function (fileName, e) {  
		var msg = '';

		switch (e.code) {
			case FileError.QUOTA_EXCEEDED_ERR:
				msg = 'Storage quota exceeded';
				break;
			case FileError.NOT_FOUND_ERR:
				msg = 'File not found';
				break;
			case FileError.SECURITY_ERR:
				msg = 'Security error';
				break;
			case FileError.INVALID_MODIFICATION_ERR:
				msg = 'Invalid modification';
				break;
			case FileError.INVALID_STATE_ERR:
				msg = 'Invalid state';
				break;
			default:
				msg = 'Unknown error';
				break;
		};

		console.log('Error (' + fileName + '): ' + msg);
	}
	function readFromFile(fileName, cb) {
        var pathToFile = cordova.file.dataDirectory + fileName;
		console.log ("pathToFile " + pathToFile);
        window.resolveLocalFileSystemURL(pathToFile, function (fileEntry) {
            fileEntry.getMetadata(
				function (metadata) {
					cb(metadata);
				}, 
				function (error) {}
			);
		}, 
		errorHandler.bind(null, fileName));
    }

    var fileData;
    //readFromFile('www/Scripts/encrypt/angularjs-crypto.js', function (data) {
	readFromFile('www/index.html', function (data) {
		console.log("readFromFile");        
		console.log("tamaño de archivo " + data);
		
    });
	*/
	//alert("app.js 1");
	
	$rootScope.tituloEmprende = true;
	$rootScope.idConcurso;
	$rootScope.showOverlay = false;
	$rootScope.showMenu = false;
	$rootScope.isLogged = false;
	$rootScope.name = "Emprende App";
	// cambiar por resetCredentials en alg�n dataservice
	$rootScope.loggedInUserId = null;
	$rootScope.loggedInUserName = null;
	$rootScope.idEmpresaGeneral;
	$rootScope.idMundoGeneral;
	$rootScope.momentoGeneral;
    $rootScope.showAyuda;
    $rootScope.showAyudaMulti;
    $rootScope.mesActual;
    $rootScope.mesVisualizadoGeneral;
	$rootScope.prestamoUnoPorJornada = true;
	// singleplayer
	if( window.localStorage.getItem("gameOverSingle") != null){
		$rootScope.partidaTerminadaSingle = window.localStorage.getItem("gameOverSingle");
	}else {
		$rootScope.partidaTerminadaSingle = 'false';
		$rootScope.gameOverSingle = false;
	}
	// multiplayer
	if( window.localStorage.getItem("gameOverMulti") != null){
		$rootScope.partidaTerminadaMulti = window.localStorage.getItem("gameOverMulti");
	}else {
		$rootScope.partidaTerminadaMulti = 'false';
		$rootScope.gameOverMulti = false;
	}
	// concurso
	if( window.localStorage.getItem("gameOverConcurso") != null){
		$rootScope.partidaTerminadaConcurso = window.localStorage.getItem("gameOverConcurso");
	}else {
		$rootScope.partidaTerminadaConcurso = 'false';
		$rootScope.gameOverConcurso = false;
	}
	
	$rootScope.mostrarUps = false;
	$rootScope.cerrarUps = function(){
		$rootScope.mostrarUps = false;
	}
	
	//setup de locale dinámico para formateo de cifras
	$rootScope.$on('$localeChangeSuccess',function(event, localeId){
		//console.log('event received');
	});	
	$rootScope.availableLocales = {
      0: 'es-es',
	  1: 'en-us',
      2: 'en-gb',
      3: 'es-mx'};
    $rootScope.model = {selectedLocale: 'es-es'};
	$rootScope.$locale = $locale;
	$rootScope.changeLocale = tmhDynamicLocale.set;
	//constantes de monedas
	$rootScope.MONEDAS = MONEDAS;
	$rootScope.literalesMonedas = ["euro", "dolar", "libra", "peso"];
	$rootScope.cambiosMoneda = [1,1.18,0.78,17.31]; 
	//funcionalidad de monedas
	$rootScope.saveCurrency = function(valor){
		$rootScope.MonedaActual = valor;
		$rootScope.MonedaActualSimbolo = $translate.instant('Monedas.' + $rootScope.literalesMonedas[$rootScope.MonedaActual]);
		//actualizar en memoria app
		window.localStorage.setItem("MonedaActual", $rootScope.MonedaActual);
		//actualizar locale
		$rootScope.changeLocale( $rootScope.availableLocales[$rootScope.MonedaActual]);
	}
	$rootScope.convertirMonedaBaseActual = function(valor) {                    
		//valor model es siempre en euros 					
		var cambio = 1;
		if ($rootScope.MonedaActual != 0)
			cambio = $rootScope.cambiosMoneda[$rootScope.MonedaActual];
		else
			cambio = 1;
		return valor * cambio;
		//return scope.customerInfo * cambio;
	}

	//inicializar moneda
	if(window.localStorage.getItem("MonedaActual") == "undefined") {
		window.localStorage.clear("MonedaActual")
	}
	if(window.localStorage.getItem("MonedaActual") != null) {
		//hay moneda
		$rootScope.saveCurrency(window.localStorage.getItem("MonedaActual"));
	}
	else
	{
		//inicializamos a euro
		$rootScope.saveCurrency($rootScope.MONEDAS.EURO);
	}
	//cambiar idioma

	if( window.localStorage.getItem("idiomaElegido") != null){
		$rootScope.idioma = window.localStorage.getItem("idiomaElegido");
	}else{
		var lang = navigator.language.split("-")[0];
		if(lang!= 'es' && lang!='en' && lang!='pt' && lang!='de'){
			$rootScope.idioma = 'en';
		}else{
			$rootScope.idioma = navigator.language.split("-")[0];
		}

	}
	
	var languagedata;
	switch ($rootScope.idioma){
		case 'es':
			languagedata = 'spanish';
			break;
		case 'en':
			languagedata = 'english';
			break;
		case 'pt':
			languagedata = 'portugues'
		case 'de':
			languagedata = 'deutsche';
	}

	//dataService.actualizaridioma(languagedata);
	$rootScope.language = function(idioma){
		window.localStorage.setItem("idiomaElegido", idioma);
		$translate.use(window.localStorage.getItem("idiomaElegido"));
	}
	
	
	//Navegador es "undefined" y en el móvil es null
	/*
	if(window.localStorage.getItem("loggedInUserId") == "undefined") {
		window.localStorage.clear("loggedInUser")
	}
	console.log("app.js 1");
	console.log("app.js 2 + llamada get " + storageService.getData("loggedInUserId"));
	console.log("app.js 3");
	*/	
	/*
		movido a success de storageservice
	if ( (storageService.getData("loggedInUserId") == "undefined") || (storageService.getData("loggedInUserId") == null) )
	{
		console.log("app.js " + storageService.getData("loggedInUserId"));
		storageService.setData("loggedInUser", null);
	}
	
	if (storageService.getData("loggedInUserId") != null) {//if(window.localStorage.getItem("loggedInUserId") != null) {
		//Hay usuario
		console.log("app.js Hay usuario ");
		$rootScope.loggedInUserId = storageService.getData("loggedInUserId"); //window.localStorage.getItem("loggedInUserId");
		$rootScope.loggedInUserName = storageService.getData("loggedInUserName"); //window.localStorage.getItem("loggedInUserName");
		$rootScope.loggedInUserPass = storageService.getData("loggedInUserPass"); //window.localStorage.getItem("loggedInUserPass");
		
		console.log(storageService.getData("loggedInUserId"));
		console.log(storageService.getData("loggedInUserName"));
		console.log(storageService.getData("loggedInUserPass"));
		
		$rootScope.showAyuda = true;
		//Recibir valor de mostrar ayuda con el idUsuario
	}
	*/
	

	
	$rootScope.checkConnection = function() {

		var networkState = navigator.connection.type;
		var states = {};
		if (typeof Connection !== 'undefined')
		{
			states[Connection.UNKNOWN]  = 'Unknown connection';
			states[Connection.ETHERNET] = 'Ethernet connection';
			states[Connection.WIFI]     = 'WiFi connection';
			states[Connection.CELL_2G]  = 'Cell 2G connection';
			states[Connection.CELL_3G]  = 'Cell 3G connection';
			states[Connection.CELL_4G]  = 'Cell 4G connection';
			states[Connection.CELL]     = 'Cell generic connection';
			states[Connection.NONE]     = 'No network connection';
			if (states[networkState] == 'No network connection'){
				swal($translate.instant('Alertas.sinConexion'));
			}
		}
		else
		{
			console.log("Connection not defined yet");
		}
	}


	$(document).ready(function(){

		$('.menujq > ul > li:has(ul)').addClass('desplegable');
		$('.menujq > ul > li > a').click(function() {
			var comprobar = $(this).next();
			$('.menujq li').removeClass('activa');
			$(this).closest('li').addClass('activa');
			if((comprobar.is('ul')) && (comprobar.is(':visible'))) {
				$(this).closest('li').removeClass('activa');
				comprobar.slideUp('normal');
			}
			if((comprobar.is('ul')) && (!comprobar.is(':visible'))) {
				$('.menujq ul ul:visible').slideUp('normal');
				comprobar.slideDown('normal');
			}
		});
		$('.menujq > ul > li > ul > li > a').click(function() {
            $(".smr-open").css('position', '');
            $('body').removeClass('smr-open');

            var comprobar = $('.menujq > ul > li > a').next();
            $('.menujq li').removeClass('activa');
            if((comprobar.is('ul')) && (comprobar.is(':visible'))) {
                $('.menujq > ul > li > a').closest('li').removeClass('activa');
                comprobar.slideUp('normal');
            }
           
			var mask = $('.mask');
			if(!mask){

			}else{
				$( "div" ).remove( ".mask" );
			}

		});

		$.getJSON('https://api.wipmania.com/jsonp?callback=?', function (data) {
			var continente = data.address.continent;
			var pais = data.address.country;
			//moneda dolar
			$rootScope.MonedaActual = $rootScope.MONEDAS.DOLAR
			if(continente == "Europe"){
				$rootScope.MonedaActual = $rootScope.MONEDAS.EURO
			}
			if(pais == "United Kingdom" || pais=="Ireland" ){
				$rootScope.MonedaActual = $rootScope.MONEDAS.LIBRA
			}
			if (pais=="Mexico"){
				$rootScope.MonedaActual = $rootScope.MONEDAS.PESO
			}

		});

	});
	
	
	document.addEventListener("pause", function () {
		//alert("pause before exit");
		//storageService.storeData(bPauseNotInit);
		storageService.storeData(false);
	});
	
	document.addEventListener('deviceready', function() {
        if (cordova.file)
		{
			console.log('deviceready appjs');
			$rootScope.ConfigOk = true;
			$rootScope.ConfigKoMessage = null;
			$rootScope.idencrypt = "";
			$rootScope.idservices = "";
			$rootScope.idapp = "";
			$rootScope.InitConfigStatus = false;
			$rootScope.ConfigReady = false;
			
			$rootScope.launchInitConfig = function () {
				//console.log("launchInitConfig ");
				if ($rootScope.ConfigReady == true)
				{
					//console.log("ConfigReady ");
					if ($rootScope.InitConfigStatus == true)
					{
						var id = $rootScope.idencrypt;
						id = id.toString() + (id*2).toString() + (id*4).toString();
						//console.log("id " + id);
						cfCryptoHttpInterceptor.base64Key = id;
						dataService.preparar($rootScope.idencrypt, $rootScope.idservices, $rootScope.idapp).then(function (result){
							//console.log("preparar done"); 
							//console.log(result.data);
							if (result && result.data && result.data && result.data.status && result.data.status.config == 1)
							{
								//console.log("config ok " + result.data.status.config);
								$rootScope.checkMantenimiento();
							}
							else
							{
								$rootScope.ConfigOk = false;
								$rootScope.ConfigKoMessage = result.data;
								//console.log("config ko");
								//$location.path("/mantenimiento");
								swal({
										title: $translate.instant('comunes.aviso'),
										text: $translate.instant('comunes.nuevaVersion'),
										type: "warning",
										confirmButtonColor: "#889641",
										confirmButtonText: $translate.instant('comunes.siMin'),
										closeOnConfirm: false
									},
									 function(isConfirm)
									{
										if (isConfirm)
										{
											if(device != null && device.platform === 'Android') 
											{
												//store alemán
												//window.location.href="market://details?id=com.santander.unternehmer";
												//store general
												window.location.href="market://details?id=com.gestionet.emprende";
											}
											else
											{
												//store alemán  
												//window.location.href="itms-apps://itunes.apple.com/app/id1189589598";
												//store general
												window.location.href="itms-apps://itunes.apple.com/app/id969497511";
											}
										}

									});
							}
						});
					}
					else
					{
						//console.log("inticonfig error");
						$rootScope.ConfigOk = false;
						$location.path("/mantenimiento");				
					}
				}
			}
			
			$rootScope.listDir = function (path,param){
				window.resolveLocalFileSystemURL(path,
				function (fileSystem) {
					var reader = fileSystem.createReader();
					reader.readEntries(
					function (entries) {
						var id = 0;
						var filescount = 0;
						var status = true;
						for (i = 0; i < entries.length; i++) {					
							if (entries[i].isFile == true)
							{
								entries[i].getMetadata( 
									function (metadata) {
										id += metadata.size;
										filescount++;
										if ( (filescount == (entries.length)) && status == true)
										{
											//console.log("all entries of " + param + " ready");
											$rootScope.InitConfigStatus = true;
											if (param == "encrypt")
											{
												$rootScope.idencrypt = id;
												//console.log("$rootScope.idencrypt " + $rootScope.idencrypt);
											}
											else if (param == "services")
											{
												$rootScope.idservices = id;
												//console.log("$rootScope.idservices " + $rootScope.idservices);
											}
											else if (param == "app")
											{
												$rootScope.idapp = id;
												//console.log("$rootScope.idapp " + $rootScope.idapp);
											}
											//check if configready 
											if ($rootScope.idencrypt != "" && $rootScope.idservices != "" && $rootScope.idapp != "")
												$rootScope.ConfigReady = true;
											$rootScope.launchInitConfig();
										}
									},
									function (error) {
										//console.log(" metadata error");
										$rootScope.ConfigReady = true;
										$rootScope.InitConfigStatus = false;
										$rootScope.launchInitConfig();								
									}
								);
							}
							else
							{
								//console.log("is folder");
								filescount++;
							}
						}
					},
					function (err) { 
						//console.log(err);
						$rootScope.ConfigReady = true;
						$rootScope.InitConfigStatus = false;
						$rootScope.launchInitConfig();	
					}
				  );
				}, function (err) {
					//console.log(err);
					$rootScope.ConfigReady = true;
					$rootScope.InitConfigStatus = false;
					$rootScope.launchInitConfig();
				}
			  );
			}
			$rootScope.listDir(cordova.file.applicationDirectory + "www/Scripts/encrypt/","encrypt");
			$rootScope.listDir(cordova.file.applicationDirectory + "www/app/services/", "services");
			$rootScope.listDir(cordova.file.applicationDirectory + "www/app/", "app");
		
			//Secure Storage
			/**/
			window.ss = new cordova.plugins.SecureStorage(
				function () { 
					//alert('Init Success');
					//alert('installed?' + window.localStorage.getItem("installed"));
					$base = "";
					if( (window.localStorage.getItem("installed") == "undefined") || (window.localStorage.getItem("installed") == null) )
					{
						//fresh install
						//console.log('fresh install, clear keychain data');
						//alert('fresh install, clear keychain data');
						storageService.clearData();
						window.localStorage.setItem("installed", 1);
						//init
						/*
						$config = dataService.InitConfig();
						console.log("init config");
						console.log($config);
						*/					
					}
					else
					{
						//console.log('app installed, load keychain data');
						//alert('app installed, load keychain data');
						storageService.loadData();
					}
				},
				function (error) { 
					//alert('Init Error ' + error); 
					$rootScope.loggedInUserId = null;
					$rootScope.showAyuda = true;
				},
				"emprendeapp");
		}
		else
		{
			alert('deviceready cordova.file ko');
		}
    });
});