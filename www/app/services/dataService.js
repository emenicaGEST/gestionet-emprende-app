/**
 * Created by Jon on 15/10/2014. 
 */
emprendeapp.factory('dataService', function ($http) {
	var IP_LOCAL = "localhost/santandersim/servidor";
	var IP_PRUEBAS = "87.98.227.218";
	var IP_PRODUCCION = "simulador.e-mprende.net"; //"emprendesim.com" //"5.196.149.8"; --> para incapsula hay que tener siempre las www en la url o da problemas
    
	var CRYPT_MODE_DEFAULT = 0;
	var CRYPT_MODE_DECRYPT = 1;
	var CRYPT_MODE_NO_DECRYPT = 2;
	var CRYPT_ACTIVATED = true;
	var CRYPT_DEACTIVATED = false;
	
	//config init
	var CONFIG_CRYPT_MODE = CRYPT_ACTIVATED;
	var CONFIG_HEADERS_CONTENT_TYPE = {'Content-Type': 'application/x-www-form-urlencoded'};
	
	if (CONFIG_CRYPT_MODE == CRYPT_ACTIVATED)
	{
		CONFIG_HEADERS_CONTENT_TYPE = {'Content-Type': 'application/json_enc'};
	}
	
	
    //var baseUrl = 'http://' + IP_PRUEBAS + '/index.php/';
	var baseUrl = 'https://' + IP_PRODUCCION + '/index.php/'; 
	var getEntornoDespliegue = function () {
        var data = {id:0, nombre:""};
		
		if (baseUrl.indexOf(IP_LOCAL) != -1)
		{
			data.id = 0;
			data.nombre = "LOCAL";
		}
		else if (baseUrl.indexOf(IP_PRUEBAS)  != -1)
		{
			data.id = 1;
			data.nombre = "PRUEBAS";
		}
		else if (baseUrl.indexOf(IP_PRODUCCION)  != -1)
		{
			data.id = 2;
			data.nombre = "PRODUCCION";
		}
		return data;
    };
	
	var getParamIndex = function (key) {
		
		var indexPrefix = key.indexOf("_");
		var postfix = key.substr(indexPrefix+1);
		var indexPostfix = postfix.indexOf("_");
		var result = postfix.substr(0,indexPostfix);
		//////console.log('getParamIndex key ' + key + " indexPrefix " + indexPrefix + " postfix " + postfix + " indexPostfix " + indexPostfix + " result " + result);
		return result;
	}
	
	var joinDecryptedParamData = function (params) {
	
		var completo = "";
		var paramname_index = "0";
		var paramdata_index = "-1";
		var paramType = 0;
		var parameters = {};
		var param = {};
		var bChanged = false;
		var bIsArray = false;
		var bDecryptMode = CRYPT_MODE_DECRYPT;
		
		params["param_X_0"] = "";
		params["data_X_0"] = "";
		
		$.each( params, function( key, value ){
			////console.log(' JOIN DECRYPT each key ' + key + " --> " + value); 
			if (key.indexOf("param_") != -1)
			{
				//param_11_0_enc
				var tempIndex = getParamIndex(key); //key.substr(6,1);
				
				////console.log(' JOIN DECRYPT checked name ' + key + " --> " + tempIndex); 
				if (paramname_index != tempIndex)
				{
					//cambio
					bChanged = true;
					//nuevo param name
					paramname_index = tempIndex;
					//guardar param data
					param['data'] = completo;
					////console.log('JOIN DECRYPT changed param name ' + completo); 
					completo = "";					
				}
			}
			if (key.indexOf("data_") != -1)
			{
				var tempIndex = getParamIndex(key); //key.substr(5,1);
				////console.log(' JOIN DECRYPT checked data ' + key + " --> " + tempIndex); 
				if (paramdata_index != tempIndex)
				{
					//nuevo param data
					paramdata_index = tempIndex;					
					////console.log(' JOIN DECRYPT changed param data ' + completo); 
					param['name'] = completo;
					completo = "";
				}
				if (key.indexOf("array") != -1)
				{
					//array
					////console.log(' JOIN DECRYPT IS ARRAY '); 
					////console.log(value); 
					value = joinDecryptedParamData(value);
					bIsArray=true;
					//bChanged=true;
				}
			}
			if (key.indexOf("mode_") != -1)
			{	
				//update decrypt mode and remove text
				var bDecryptMode = value;
				value = "";
			}
			else if (key!="base")
			{
				if (bChanged)
				{
					//guardar param
					////console.log(' JOIN DECRYPT GUARDAR '); 
					////console.log('NAME ' + param['name']); 
					////console.log('DATA '); 
					////console.log(param['data']); 
					parameters[param['name']] = param['data'];
					param = {};
					//reset vars
					bChanged = false;				
				}
				if (!bIsArray)
				{
					completo += value;
				}
				else
				{
					completo = value;
					bIsArray=false;
				}
			}
		});	
		////console.log(' JOIN DECRYPT completo --> ');
		////console.log(parameters);
		return parameters;
	}
	
	
	var splitEncryptedParamData = function (params) {
		//initialize vars
		var size = 7;
		var array = {};
		var dataarray = [];
		var re = new RegExp('.{1,' + size + '}', 'g');
		var name = "";
		var data = "";
		
		for(var index=0; index<params.length; index++) {
			//////console.log("parameter " + index);
			////console.log(params[index]);
			
			name = params[index]['name']['value'];
			data = params[index]['data']['value'] + ""; //convert to string
			//check decrypt mode for name
			if (params[index]['name']['mode'])
			{	
				//////console.log(" mode " + params[index]['name']['mode']);
				var id = "mode_"+index;
				array[id] = params[index]['name']['mode'];
			}
			
			//split param name
			var dataarray = name.match(re);
			//////console.log("name array " + dataarray[0]);
			for(var i=0; i<dataarray.length; i++) {
				var id = "param_"+index+"_"+i+"_enc";
				array[id] = dataarray[i];
			};
			//check decrypt mode for data
			if (params[index]['data']['mode'])
			{
				//////console.log(" mode " + params[index]['data']['mode']);
				var id = "mode_"+index;
				array[id] = params[index]['data']['mode'];
			}
			//check if empty
			if ((data != null) && (data != ""))
			{
				
				//split param data
				dataarray = data.match(re);
				//////console.log("data array " + dataarray[0]);		
				for(var i=0; i<dataarray.length; i++) {
					var id = "data_"+index+"_"+i+"_enc";
					array[id] = dataarray[i];
				};
			}
			else
			{
				////console.log(" EMPTY DATA ");
				var id = "data_"+index+"_0_enc";
				array[id] = "";
			}
		};
		////console.log(' SPLIT ENCRYPT completo --> ');
		////console.log(array);
		return array;
	};
	
    var validateUser = function (loginUsername, loginPassword) {
		
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//loginUsername
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "loginUsername";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = loginUsername;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			//loginPassword
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = "loginPassword";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = loginPassword;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "loginUsername="+loginUsername+"&loginPassword="+loginPassword;
		}
	
		
		return $http({
            withCredentials: false,
			crypt: true,
            method: 'post',
            url: baseUrl+"login/mobile",
            //headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            //data: "loginUsername="+user+"&loginPassword="+password
			data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL validateUser");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    
    var registro = function(nick, nombre, apellidos, email, pais, password, idConcurso){
		
		//init 
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//nick
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "nick";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = nick;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			//nombre
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = "nombre";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = nombre;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			//apellidos
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = "apellidos";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = apellidos;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			//email
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = "email";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = email;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			//pais
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = "pais";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = pais;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			//password
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = "password";
			param['name'] = paramMode;
			paramMode = {};
			//paramMode['mode'] = CRYPT_MODE_NO_DECRYPT;
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = password;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			//idConcurso
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idConcurso";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idConcurso;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "nick="+nick+"&nombre="+nombre+"&apellidos="+apellidos+"&pais="+pais+"&email="+email+"&password="+password+"&idConcurso="+idConcurso;
		}
	
        return $http({
            withCredentials: false,
			crypt: true,
            method: 'post',
            url: baseUrl+"registro",
			data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL registro");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };

    var continuar = function(user){
        var on = "ocultar";
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//userId
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "userId";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = userId;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			//ocultarAsistente
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "ocultarAsistente";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = on;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "userId="+user+"&ocultarAsistente="+on;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"finalizarAsistente/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL continuar");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

     var checkPartidaExistente = function(userId, tipoPartida){
		
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//userId
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "userId";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = userId;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			//tipoPartida
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipoPartida";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipoPartida;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "userId="+userId+"&tipoPartida="+tipoPartida;
		}
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"menuprincipal/checkHayPartida/mobile",
			data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL checkPartidaExistente");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };

    var terminarEmpezarPartida = function(idEmpresa, idMundo, tipoPartida){
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			//idMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			//tipoPartida
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipoPartida";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipoPartida;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idMundo="+idMundo+"&tipoPartida="+tipoPartida;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"menuprincipal/terminarPartida/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL terminarEmpezarPartida");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var crearNuevaPartida = function(tipocaso,dificultad,tipoPartida,tipoImpositivo,userId){
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//tipocaso
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipocaso";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipocaso;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//dificultad
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "dificultad";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = dificultad;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//tipoPartida
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipoPartida";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipoPartida;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//tipoImpositivo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipoImpositivo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipoImpositivo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//userId
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "userId";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = userId;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "tipocaso="+tipocaso+"&dificultad="+dificultad+"&tipopartida="+tipoPartida+"&tipoImpositivo="+tipoImpositivo+"&userId="+userId;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"menuprincipal/nuevapartida/crearpartida/mobile",
			data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL crearNuevaPartida");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };
	
    var cargarPartida = function(userId,tipoPartida){
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//userId
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "userId";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = userId;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//tipoPartida
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipoPartida";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipoPartida;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "userId="+userId+"&tipoPartida="+tipoPartida;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"menuprincipal/continuarpartida/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL cargarPartida");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var valoraciones = function(idEmpresa,momento){
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
		
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+momentoGeneral+"&momento="+momento;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"menuprincipal/getValoraciones/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL valoraciones");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var get_MomentoActualApp = function(idEmpresa){
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"menuprincipal/getMomentoActual/mobile",
			data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL get_MomentoActualApp");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };
	
    var getBalance = function(idEmpresa,idMundo,momento){
		
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idMundo="+idMundo+"&momento="+momento;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"balance/CalcularBalance/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getBalance");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var getPyG = function(idEmpresa,idMundo,momento){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idMundo="+idMundo+"&momento="+momento;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"PYG/CalcularPYG/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getPYG");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var getEFE = function(idEmpresa,idMundo,momento){
	
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idMundo="+idMundo+"&momento="+momento;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"EFE/CalcularEFE/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getEFE");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var getFondosInversion = function(idEmpresa,idMundo,momento){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idMundo="+idMundo+"&momento="+momento;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"FI/ObtenerFondos/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getFondosInversion");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var getCantidadDisponible = function(idEmpresa,idMundo,momento){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idMundo="+idMundo+"&momento="+momento;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"FI/CantidadDisponible/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getCantidadDisponible");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var guardarFI = function(idEmpresa,idMundo,momento, datos){
	
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//datos
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "datos";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = datos;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idMundo="+idMundo+"&momento="+momento+"&datos="+datos;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"FI/GuardarFI/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL GuardarFI");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var getDataPrestamos = function(idEmpresa,idMundo,momento){
	
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idMundo="+idMundo+"&momento="+momento;
		}
			
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"NP/GetDataPrestamos/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getDataPrestamos");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var pedirPrestamos = function(idEmpresa,idMundo,momento,datos){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//datos
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "datos";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = datos;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"idMundo="+idMundo+"&momento="+momento+"&datos="+datos;
		}		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"NP/Pedir/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL pedirPrestamos");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var cancelarPrestamos = function(datos){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//datos
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "datos";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = datos;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "datos="+datos;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"NP/Cancel/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL cancelarPrestamos");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var descancelarPrestamos = function(datos){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//datos
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "datos";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = datos;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "datos="+datos;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"NP/UnCancel/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL descancelarPrestamos");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var obtenerSubvencionesDisponibles = function(idEmpresa,momento){
        //init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&momento="+momento;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"subvenciones/getDisponibles/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL obtenerSubvencionesDisponibles");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var obtenerSubvencionesSolicitadas = function(idEmpresa,momento){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&momento="+momento;
		}
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"subvenciones/getSolicitadas/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL obtenerSubvencionesSolicitadas");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var solicitarSubvencion = function(idEmpresa,idSubvencion){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idSubvencion
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idSubvencion";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idSubvencion;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idSubvencion="+idSubvencion;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"subvenciones/solicitar/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL solicitarSubvencion");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var getProveedores = function(){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			
		}
		else
		{
			processedParams = "";
		}
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"proveedores/getProveedores/mobile",
			data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getProveedores");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var getDisponibilidadMaterial = function(idEmpresa, idMundo, momento, pantalla){
		
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//pantalla
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "pantalla";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = pantalla;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idMundo="+idMundo+"&momento="+momento+"&pantalla="+pantalla;
		}
			
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"proveedores/getDisponibilidad/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getDisponibilidadMaterial");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var comprarProveedores = function(idEmpresa,momento,idMundo, datos){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//datos
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "datos";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = datos;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&momento="+momento+"&idMundo="+idMundo+"&datos="+datos;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"proveedores/pedir/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL comprarProveedores");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var obtenerCoberturas = function(idEmpresa,momento){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&momento="+momento;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"medios/getMedios/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL obtenerCoberturas");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

	var actualizarCobertura = function(medios){
		
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//medios
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "medios";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = medios;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "medios="+medios;
		}
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"medios/actualizar/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL actualizarCobertura");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

	 var getTotalPoblacion = function(idEmpresa,perfil,estado){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//perfil
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "perfil";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = perfil;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//estado
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "estado";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = estado;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"perfil="+perfil+"estado="+estado;
		}
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"PL/getTotalPoblacion/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getTotalPoblacion");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
	
    var getPoblacion = function(idEmpresa,perfil,estado){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//perfil
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "perfil";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = perfil;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//estado
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "estado";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = estado;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"perfil="+perfil+"estado="+estado;
		}
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"PL/getPoblacion/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getPoblacion");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
	
    var getContratados = function(idEmpresa,perfil,estado, momento){
		
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//perfil
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "perfil";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = perfil;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//estado
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "estado";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = estado;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&perfil="+perfil+"&estado="+estado+"&momento="+momento;
		}
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"PL/getContratados/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getContratados");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var contratar = function(idEmpresa,idPoblacion,tipoContrato, salario, vacaciones, horarios, momento){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idPoblacion
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idPoblacion";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idPoblacion;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//tipoContrato
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipoContrato";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipoContrato;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//salario
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "salario";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = salario;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//vacaciones
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "vacaciones";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = vacaciones;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//horarios
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "horarios";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = horarios;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idPoblacion="+idPoblacion+"&tipoContrato="+tipoContrato+"&salario="+salario+"&vacaciones="+vacaciones+"&horarios="+horarios+"&momento="+momento;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"PL/Contratar/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL contratar");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var despedir = function(idEmpresa,idPoblacion,momento ){
	
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idPoblacion
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idPoblacion";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idPoblacion;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idPoblacion="+idPoblacion+"&momento="+momento;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"PL/Despedir/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL despedir");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var formar = function(idEmpresa,idPoblacion,valor,momento){
		
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idPoblacion
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idPoblacion";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idPoblacion;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//valor
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "valor";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = valor;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idPoblacion="+idPoblacion+"&valor="+valor+"&momento="+momento;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"PL/Formar/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL formar");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };


    var asignarVacaciones = function(idEmpresa,idPoblacion,valor,momento){
		
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idPoblacion
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idPoblacion";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idPoblacion;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//valor
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "valor";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = valor;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idPoblacion="+idPoblacion+"&valor="+valor+"&momento="+momento;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"PL/asignarVacaciones/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL asignarVacaciones");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
	
    var asignarHorarios = function(idEmpresa,idPoblacion,valor,momento){
		
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idPoblacion
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idPoblacion";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idPoblacion;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//valor
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "valor";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = valor;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idPoblacion="+idPoblacion+"&valor="+valor+"&momento="+momento;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"PL/asignarHorarios/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL asignarHorarios");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var asignarSalarioActual = function(idEmpresa,idPoblacion,valor){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idPoblacion
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idPoblacion";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idPoblacion;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//valor
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "valor";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = valor;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idPoblacion="+idPoblacion+"&valor="+valor;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"PL/asignarSalarioActual/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL asignarSalarioActual");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var getProyectosSolicitables = function(idEmpresa,momento){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&momento="+momento;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"proyectos/getDisponibles/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getProyectosSolicitables");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var solicitarProyecto  = function(idEmpresa,idProyecto,presupuesto,momento){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idProyecto
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idProyecto";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idProyecto;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//presupuesto
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "presupuesto";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = presupuesto;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idProyecto="+idProyecto+"&presupuesto="+presupuesto+"&momento="+momento;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"proyectos/solicitar/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL solicitarProyecto");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    
    var getProyectosEnCurso = function(idEmpresa,momento, momentoMundo){
	
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momentoMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momentoMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momentoMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&momento="+momento+"&momentoMundo="+momentoMundo;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"proyectos/getAdjudicados/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getProyectosEnCurso");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var cancelarProyecto = function(idEmpresa,idProyecto, momento){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idProyecto
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idProyecto";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idProyecto;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idProyecto="+idProyecto+"&momento="+momento;
		}
				
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"proyectos/cancelar/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL cancelarProyecto");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var asignarHorasProyecto = function(idEmpresa, idProyecto, horasDise, horasProg){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idProyecto
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idProyecto";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idProyecto;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//horasDise
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "horasDise";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = horasDise;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//horasProg
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "horasProg";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = horasProg;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idProyecto="+idProyecto+"&horasDise="+horasDise+"&horasProg="+horasProg;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"proyectos/asignarHorasProyecto/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL asignarHorasProyecto");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var getInfoMercado = function(idEmpresa,momento){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&momento="+momento;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"infomercado/getInformacion/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getInformacion");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var pasarJornada = function(idMundo,momento){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idMundo="+idMundo+"&momento="+momento;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"mercados/PasaMundo/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL pasarJornada");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var getTotalAlertasNuevas = function(idEmpresa){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"alertas/getTotalAlertasNuevas/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getTotalAlertasNuevas");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var getNotificaciones = function(idEmpresa,languagedata){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//languagedata
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "languagedata";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = languagedata;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"alertas/getNotificaciones/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getNotificaciones");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var marcarAlertasLeidas = function(idEmpresa){
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"alertas/marcarAlertasLeidas/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL marcarAlertasLeidas");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };

    var createUser = function(){
		var processedParams={};
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			
		}
		else
		{
			processedParams = "";
		}
        return $http({
            withCredentials: false,
            method: 'post',
			url: baseUrl+"generarUsuario/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL createUser");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };

    var getRankingIndividual= function(userId, maxResultados, tipo, tipoPartida) {
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//userId
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "userId";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = userId;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//maxResultados
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "maxResultados";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = maxResultados;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//tipo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//tipoPartida
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipoPartida";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipoPartida;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "userId=" + userId + "&maxResultados=" + maxResultados + "&tipo=" + tipo + "&tipoPartida=" + tipoPartida;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl + "menuprincipal/ranking/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL GetRankingIndividual");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var getRanking= function(userId, maxResultados, tipo, situacion, dificultad, tipoPartida) {
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//userId
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "userId";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = userId;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//maxResultados
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "maxResultados";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = maxResultados;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//tipo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//situacion
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "situacion";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = situacion;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//dificultad
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "dificultad";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = dificultad;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//tipoPartida
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipoPartida";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipoPartida;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "userId=" + userId + "&maxResultados=" + maxResultados + "&tipo=" + tipo + "&situacion=" + situacion + "&dificultad=" + dificultad + "&tipoPartida=" + tipoPartida;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl + "menuprincipal/ranking/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getRanking");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
     var getRankingMulti= function(userId, maxResultados, tipo, tipoPartida, maxDecisionesEmpresa) {
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//userId
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "userId";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = userId;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//maxResultados
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "maxResultados";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = maxResultados;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//tipo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//tipoPartida
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipoPartida";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipoPartida;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//maxDecisionesEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "maxDecisionesEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = maxDecisionesEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "userId=" + userId + "&maxResultados=" + maxResultados + "&tipo=" + tipo + "&tipoPartida=" + tipoPartida + "&maxDecisionesEmpresa=" + maxDecisionesEmpresa;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl + "menuprincipal/ranking/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getRankingMulti");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var getHistoricoMercado = function(idEmpresa) {
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
									
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl + "infomercado/getHistorico/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getHistoricoMercado");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
	
	
	var getHistoricoCompetidores = function(idMundo, idEmpresa, momento) {
        
		//init 
		var processedParams = {};
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//momento
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "momento";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = momento;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
									
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idMundo="+idMundo + "&idEmpresa="+idEmpresa + "&momento=" + momento;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl + "infomercado/getHistoricoCompetidores/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getHistoricoCompetidores");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
	
	
    var actualizaridioma = function(languagedata) {
	
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//languagedata
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "languagedata";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = languagedata;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "languagedata="+languagedata;
		}
		
		
        return $http({
            withCredentials: false,
			method: 'post',
            url: baseUrl + "actualizaridioma/mobile",
            //data: "languagedata="+languagedata,
			//headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			//data: processedParams,
            //headers: {'Content-Type': 'application/json_enc'}
			data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL actualizaridioma");
			////console.log(result);
			//console.log("----------------------------------------");
			if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);			
			return result;
        });
    };
    var getInfoMantenimiento = function(status) {
		
		//init
		var processedParams = {};
		var legacy = "1.3";
		if (status == "undefined" || status == false)
			legacy = "0.0";
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//languagedata
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "legacy";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = legacy;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			//config			
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "initConfigKo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = status;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "legacy="+legacy;
		}
		
        return $http({
            withCredentials: false,
			method: 'post',
            url: baseUrl + "getInfoMantenimiento/mobile",
			data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getInfoMantenimiento");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };
	
	
	var preparar = function (ida,idb,idc){
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//id
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idencrypt";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = ida;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idb
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idservices";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idb;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idc
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idapp";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idc;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "";
		}
        return $http({
            withCredentials: false,
			method: 'post',
            url: baseUrl + "initConfig/mobile",
			data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL initConfig");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			//alert(result.data);
			return result;			
        });
    };
	
	
	/*MULTIJUGADOR*/
    var getPartidasMultiDisponibles = function(){
	
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
		
		}
		else
		{
			processedParams = "";
		}
		
        return $http({
            withCredentials: false,
			method: 'post',
            url: baseUrl+"menuprincipal/getPartidasMultijugador/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getPartidasMultiDisponibles");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };
	var unirseAPartidaMulti = function(idMundo, userId, tipopartida){
        
		//init
		var processedParams = {};
		var legacy = "1.3";
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//userId
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "userId";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = userId;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//tipopartida
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipopartida";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipopartida;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idMundo="+idMundo + "&userId=" + userId + "&tipopartida=" + tipopartida;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"menuprincipal/unirseAPartidaMulti/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL unirseAPartidaMulti");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };	

	var salirDePartidaMulti = function(idEmpresa, idMundo, tipoPartida){
        
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresa;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//tipoPartida
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipoPartida";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipoPartida;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresa+"&idMundo="+idMundo+"&tipoPartida="+tipoPartida;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"menuprincipal/salirDePartidaMulti/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL salirDePartidaMulti");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };	
	
	var getInfoMercadoMulti = function(idMundoGeneral, idEmpresaGeneral){
        
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idMundoGeneral
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundoGeneral";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundoGeneral;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresaGeneral;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idMundoGeneral="+idMundoGeneral+"&idEmpresa="+idEmpresaGeneral;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"menuprincipal/getInfoMercadoMulti/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getInfoMercadoMulti");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };	
	
	var activarEmpresa = function(idEmpresaGeneral, tipoPartida, idMundo){
        
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idEmpresa
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idEmpresa";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idEmpresaGeneral;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//tipoPartida
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipoPartida";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipoPartida;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idMundo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idMundo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idMundo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idEmpresa="+idEmpresaGeneral+"&tipoPartida="+tipoPartida+"&idMundo="+idMundo;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"menuprincipal/activarEmpresa/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL activarEmpresa");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };	
	
	var checkUserExistente = function(userId){
		
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//userId
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "userId";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = userId;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "userId="+userId;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"menuprincipal/checkExisteUser/mobile",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL checkExisteUser");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };
    var getPaisesConcurso = function(){
        
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{

		}
		else
		{
			processedParams = "";
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"getPaisesConcurso",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getPaisesConcurso");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };
    var getConcursosActivos = function(){
        
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
		
		}
		else
		{
			processedParams = "";
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"getConcursosActivos",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getConcursosActivos");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };
    var getFasesConcurso = function(idConcurso){
        
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idConcurso
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idConcurso";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idConcurso;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idConcurso="+idConcurso;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"getFasesConcurso",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getFasesConcurso");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };
    var getEmpresaUserConcurso = function(idConcurso, idUsuario){
        
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idConcurso
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idConcurso";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idConcurso;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idUsuario
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idUsuario";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idUsuario;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idConcurso="+idConcurso+"&idUsuario="+idUsuario;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"getEmpresaUserConcurso",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getEmpresaUserConcurso");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };
	var getUserConcurso = function(idConcurso, idUsuario){
        
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idConcurso
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idConcurso";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idConcurso;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idUsuario
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idUsuario";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idUsuario;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
						
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idConcurso="+idConcurso+"&idUsuario="+idUsuario;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"getUserConcurso",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getUserConcurso");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };
	
    var getPremiosConcurso = function(idConcurso){
        
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//idConcurso
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idConcurso";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idConcurso;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
									
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idConcurso="+idConcurso;
		}
		
		return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"getPremiosConcurso",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getPremiosConcurso");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };

    var solicitarResetPwd = function(mail){
		
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//mail
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "mail";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = mail;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "mail="+mail;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"solicitarResetPwd",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL solicitarResetPwd");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };
    var comprobar_reset_pwd_code = function(mail, code){
		
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
			
			//MAIL
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "mail";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = mail;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			//code
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "code";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = code;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "mail="+mail;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"comprobar_reset_pwd_code",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL comprobar_reset_pwd_code");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };
    var update_password = function(mail, password){
		
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
					
			//MAIL
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "mail";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = mail;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			//code
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "password";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = password;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "mail="+mail+"&password="+password;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"update_password",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL update_password");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };
    var getSorteosConcurso = function(idConcurso, idUsuario){
		
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
					
			//idConcurso
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idConcurso";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idConcurso;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			//idUsuario
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idUsuario";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idUsuario;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "idConcurso="+idConcurso+"&idUsuario="+idUsuario;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"getSorteosConcurso",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getSorteosConcurso");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };
    var getRankingConcurso = function(idConcurso, userId, idConcursoMundoBase, idPais, maxResultados, tipo, esFinal){
		
		//init
		var processedParams = {};
		
		if (CONFIG_CRYPT_MODE)
		{
			var parameters = [];
			var param = {};
			var paramMode = {};
					
			//userId
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "userId";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = userId;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};			
			//idConcurso
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idConcurso";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idConcurso;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idConcursoMundoBase
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idConcursoMundoBase";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idConcursoMundoBase;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//idPais
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "idPais";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = idPais;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//maxResultados
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "maxResultados";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = maxResultados;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//tipo
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "tipo";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = tipo;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			//esFinal
			paramMode['mode'] = CRYPT_MODE_DECRYPT;
			paramMode['value'] = "esFinal";
			param['name'] = paramMode;
			paramMode = {};
			paramMode['mode'] = CRYPT_MODE_DEFAULT;
			paramMode['value'] = esFinal;
			param['data'] = paramMode;
			paramMode = {};
			parameters.push(param);
			param = {};
			
			processedParams = splitEncryptedParamData(parameters);
		}
		else
		{
			processedParams = "userId="+userId+"&idConcurso="+idConcurso+"&idConcursoMundoBase="+idConcursoMundoBase+"&idPais="+idPais+"&maxResultados="+maxResultados+"&tipo="+tipo+"&esFinal="+esFinal;
		}
		
        return $http({
            withCredentials: false,
            method: 'post',
            url: baseUrl+"getRankingConcurso",
            data: processedParams,
			crypt: CONFIG_CRYPT_MODE,
			headers: CONFIG_HEADERS_CONTENT_TYPE
        }).then(function (result) {
			//console.log("----------------------------------------");
			//console.log("CALL getRankingConcurso");
			////console.log(result);
			//console.log("----------------------------------------");
           if (CONFIG_CRYPT_MODE)
				result.data = joinDecryptedParamData(result.data);
			return result;
        });
    };

    var service = {
		getEntornoDespliegue: getEntornoDespliegue,
        validateUser: validateUser,
        registro: registro,
        continuar: continuar,
        checkPartidaExistente: checkPartidaExistente,
        crearNuevaPartida: crearNuevaPartida,
        terminarEmpezarPartida: terminarEmpezarPartida,
        cargarPartida: cargarPartida,
        valoraciones: valoraciones,
        get_MomentoActualApp: get_MomentoActualApp,
        getBalance: getBalance,
        getPyG:getPyG,
        getEFE: getEFE,
        getFondosInversion: getFondosInversion,
        getCantidadDisponible:getCantidadDisponible,
        guardarFI:guardarFI,
        getDataPrestamos: getDataPrestamos,
        pedirPrestamos: pedirPrestamos,
        cancelarPrestamos: cancelarPrestamos,
        descancelarPrestamos: descancelarPrestamos,
        obtenerSubvencionesDisponibles: obtenerSubvencionesDisponibles,
        obtenerSubvencionesSolicitadas: obtenerSubvencionesSolicitadas,
        solicitarSubvencion: solicitarSubvencion,
        getProveedores: getProveedores,
        getDisponibilidadMaterial: getDisponibilidadMaterial,
        comprarProveedores: comprarProveedores,
        obtenerCoberturas: obtenerCoberturas,
        actualizarCobertura: actualizarCobertura,
        getPoblacion: getPoblacion,
		getTotalPoblacion: getTotalPoblacion,
        getContratados: getContratados,
        contratar: contratar,
        despedir: despedir,
        formar: formar,
        asignarVacaciones: asignarVacaciones,
        asignarHorarios: asignarHorarios,
        asignarSalarioActual: asignarSalarioActual,
        getProyectosSolicitables: getProyectosSolicitables,
        solicitarProyecto: solicitarProyecto,
        getProyectosEnCurso: getProyectosEnCurso,
        cancelarProyecto: cancelarProyecto,
        asignarHorasProyecto: asignarHorasProyecto,
        getInfoMercado: getInfoMercado,
        pasarJornada: pasarJornada,
        getTotalAlertasNuevas: getTotalAlertasNuevas,
        getNotificaciones: getNotificaciones,
        marcarAlertasLeidas: marcarAlertasLeidas,
        createUser: createUser,
        getRankingIndividual: getRankingIndividual,
        getHistoricoMercado: getHistoricoMercado,
		getHistoricoCompetidores: getHistoricoCompetidores,
        actualizaridioma: actualizaridioma,
        getInfoMantenimiento: getInfoMantenimiento,
        getRanking: getRanking,
		getPartidasMultiDisponibles: getPartidasMultiDisponibles,
		unirseAPartidaMulti: unirseAPartidaMulti,
		salirDePartidaMulti: salirDePartidaMulti,
		getInfoMercadoMulti: getInfoMercadoMulti,
		activarEmpresa: activarEmpresa,
		checkUserExistente: checkUserExistente,
        getRankingMulti: getRankingMulti,
        getPaisesConcurso: getPaisesConcurso,
        getConcursosActivos: getConcursosActivos,
        getFasesConcurso: getFasesConcurso,
		getUserConcurso: getUserConcurso,
        getEmpresaUserConcurso: getEmpresaUserConcurso,		
        getPremiosConcurso: getPremiosConcurso,
        solicitarResetPwd: solicitarResetPwd,
        comprobar_reset_pwd_code: comprobar_reset_pwd_code,
        update_password: update_password,
        getSorteosConcurso: getSorteosConcurso,
        getRankingConcurso: getRankingConcurso,
		splitEncryptedParamData: splitEncryptedParamData,
		joinDecryptedParamData: joinDecryptedParamData,
		getParamIndex: getParamIndex,
		preparar: preparar
    };
    return service;
});