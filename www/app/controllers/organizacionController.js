emprendeapp.controller('organizacionController', ['$scope', 'dataService','$rootScope','$location','$translate', '$filter', function ($scope, dataService, $rootScope,$location, $translate, $filter) {
    $scope.perfil = 2; // diseñador
    $scope.estado = 1; // disponible
    $scope.formacion;
    $scope.formOrganizacion;
    $scope.coste = 1.35;
    $scope.salarioOfr;
    $scope.checkDisenador = true;
    $scope.checkProgramador = false;
    $scope.costeSalarial;
    var cambioVacas = false;
    var cambioHoras = false;
    var cambioFormacion = false;
    var cambioSalario = false;
	$scope.salarioMinimo = 600;
	
	$scope.salarioOfr=0;
	$scope.salarioOfrConvertido=0;

	
	$scope.contratadosInfo = [];
	
    $scope.cambioVacaciones = function () {
        cambioVacas = true
    };

    $scope.cambiandoHoras = function () {
        cambioHoras = true
    };


    $scope.cambiandoFormacion = function (formacion) {
        if (formacion == 1){
            swal($translate.instant('Alertas.orgFormacion'));
        }
        cambioFormacion = true
    };

    $scope.cambiandoSalario = function () {
        cambioSalario = true
    };
    $scope.pantallaInformacion = function(){
        $location.path('informacion/pantalla/nombreOrganizacion/parrafo_unoOrganizacion/parrafo_dosOrganizacion');
    };

    $scope.vacacionesAsignadas = [];
    for (var i = 0; i < 12; i++) {
        $scope.vacacionesAsignadas.push($translate.instant('Organizacion.FechaMeses'))
    };
    //Cargar diseñadores según entras
    dataService.getContratados($rootScope.idEmpresaGeneral,$scope.perfil,$scope.estado, $rootScope.momentoGeneral).then(function (result) {
        $scope.contratadosInfoObject = result.data.datos;
		$scope.contratadosInfo = objetcToArray($scope.contratadosInfo, $scope.contratadosInfoObject);
		$scope.salarioMinimo = result.data.salarioMinimo;
        $scope.ningunContratado = $scope.contratadosInfo.length == 0;
    });

    $scope.contratados = function(perfil){
        $scope.perfil = perfil;
        $scope.estado = 1;
        dataService.getContratados($rootScope.idEmpresaGeneral,$scope.perfil,$scope.estado, $rootScope.momentoGeneral).then(function (result) {
            $scope.contratadosInfoObject = result.data.datos;
			$scope.contratadosInfo = objetcToArray($scope.contratadosInfo, $scope.contratadosInfoObject);
            $scope.ningunContratado = $scope.contratadosInfo.length == 0;
        });

        if(perfil == 1) {
            $scope.checkDisenador = false;
            $scope.checkProgramador = true;

        } else if(perfil == 2) {
            $scope.checkDisenador = true;
            $scope.checkProgramador = false;
        }
    },

    $scope.guardar = function(idPoblacion, horario,vacaciones, salarioOfr, formacion, perfil, tipoContrato, salarioOfrSinModificar, vacacionesMes){
        $scope.errorOrgPorcentaje= "";
        $scope.errorOrgFormVac= "";
        $scope.errorOrgParcialHoras= "";
		$scope.errorContSalarioMin = "";
        bDatosCorrectos = true;
		salarioOfrSinModificar = parseFloat(salarioOfrSinModificar);
        $scope.porcentaje = parseFloat(salarioOfrSinModificar * 20 / 100); // saco el porcentaje que puede variar como máximo
        //console.log("salarioOfrSinModificar" + salarioOfrSinModificar);
		$scope.totalPorcentajeArriba = salarioOfrSinModificar + $scope.porcentaje;
        $scope.totalPorcentajeAbajo = salarioOfrSinModificar - $scope.porcentaje;
        //console.log("Porcentaje" + $scope.porcentaje);
        //console.log("totalPorcentajeArriba" + $scope.totalPorcentajeArriba);
        //console.log("totalPorcentajeAbajo" + $scope.totalPorcentajeAbajo);
		
		var jornadaActual = $rootScope.momentoGeneral;
		if ($rootScope.tipoPartida != 1)
			jornadaActual = $rootScope.mundoMultiData.mundo.jornadaActual;
        var vacacionesMes = parseInt(vacacionesMes);// - 1;
        var vacacionesPedidas =  parseInt(vacaciones);// - 1;
		//console.log("$rootScope.momentoGeneral " + $rootScope.devolverMesIndice($rootScope.momentoGeneral));
        //console.log("vacacionesPedidas " + vacacionesPedidas);
        //console.log("vacacionesMes " + vacacionesMes);
        if(salarioOfr < $scope.salarioMinimo) {
            $scope.errorContSalarioMin = $translate.instant('Ups.errorContSalarioMin');
            $rootScope.mostrarUps = true;
            bDatosCorrectos = false;            
        }else if(salarioOfr > $scope.totalPorcentajeArriba || salarioOfr < $scope.totalPorcentajeAbajo  ){
            $scope.errorOrgPorcentaje = $translate.instant('Ups.errorOrgPorcentaje') + $filter('customCurrency')(salarioOfrSinModificar,"€",",",".",0);
            $rootScope.mostrarUps = true;
            bDatosCorrectos = false;
        }else if((formacion == 1 && vacacionesMes == parseInt($rootScope.devolverMesIndice(jornadaActual)) ) || (formacion == 1 && vacacionesPedidas ==  $rootScope.devolverMesIndice(jornadaActual))) {
            //console.log("vacacionesMes formacion error");
			$scope.errorOrgFormVac = $translate.instant('Ups.errorOrgFormVac');
            $rootScope.mostrarUps = true;
            bDatosCorrectos = false;
        }
		/*
		else if((formacion == 1 && vacacionesMes == 1 && $rootScope.momentoGeneral == 12) || (formacion == 1 && vacacionesMes == 2 && $rootScope.momentoGeneral == 13)|| (formacion == 1 && vacacionesMes == 3 && $rootScope.momentoGeneral == 14)) {
            $scope.errorOrgFormVac = $translate.instant('Ups.errorOrgFormVac');
            $rootScope.mostrarUps = true;
            bDatosCorrectos = false;
        }else if((formacion == 1 && vacacionesPedidas == 1 && $rootScope.momentoGeneral == 12) || (formacion == 1 && vacacionesPedidas == 2 && $rootScope.momentoGeneral == 13)|| (formacion == 1 && vacacionesPedidas == 3 && $rootScope.momentoGeneral == 14)) {
            $scope.errorOrgFormVac = $translate.instant('Ups.errorOrgFormVac');
            $rootScope.mostrarUps = true;
            bDatosCorrectos = false;
        }
		*/
		else if((tipoContrato == 2) && (horario > 5) ){
            $scope.errorOrgParcialHoras = $translate.instant('Ups.errorOrgParcialHoras');
            $rootScope.mostrarUps = true;
            bDatosCorrectos = false;
        }else{
            asignarVacaciones(idPoblacion, vacaciones, perfil);
            asignarSalarioActual(idPoblacion, salarioOfr, perfil);
            asignarHorarios(idPoblacion, horario, perfil);
            //formación-  si= 1, no= 0
            formar(idPoblacion, formacion, perfil);
        }

    };

    var asignarSalarioActual = function(idPoblacion, salarioOfr, perfil){
        $scope.idPoblacion = idPoblacion;
        $scope.valor = salarioOfr;
        $scope.perfil = perfil;

        if(!cambioSalario) {
            return;
        }

        dataService.asignarSalarioActual($rootScope.idEmpresaGeneral,$scope.idPoblacion,$scope.valor).then(function (result) {
            $scope.contratados($scope.perfil);
            swal($translate.instant('Alertas.operacionCorrecta'));
        });
    };

    var asignarVacaciones =  function(idPoblacion, vacaciones, perfil){
        $scope.idPoblacion = idPoblacion;
        $scope.valor = vacaciones;
        $scope.perfil = perfil;

        if(!cambioVacas) {
            return;
        }

        dataService.asignarVacaciones($rootScope.idEmpresaGeneral,$scope.idPoblacion,$scope.valor, $rootScope.momentoGeneral).then(function (result) {
            $scope.contratados($scope.perfil);
             if (result.data.alertas == true) {
                dataService.getTotalAlertasNuevas($rootScope.idEmpresaGeneral).then(function (result) {
                    $rootScope.totalAlertas = result.data.datos;
                });
            }
			swal($translate.instant('Alertas.operacionCorrecta'));
            if($rootScope.horasAsignadasProyecto == true){
                swal($translate.instant('Alertas.orgCambios'));
            }
            $rootScope.horasAsignadasProyecto = false;

        });
    };
    var asignarHorarios = function(idPoblacion, horario, perfil){
        $scope.idPoblacion = idPoblacion;
        $scope.valor = horario;
        $scope.perfil = perfil;

        if(!cambioHoras) {
            return;
        }

        dataService.asignarHorarios($rootScope.idEmpresaGeneral,$scope.idPoblacion,$scope.valor, $rootScope.momentoGeneral).then(function (result) {
            $scope.contratados($scope.perfil);
            if (result.data.alertas == true) {
                dataService.getTotalAlertasNuevas($rootScope.idEmpresaGeneral).then(function (result) {
                    $rootScope.totalAlertas = result.data.datos;
                });
            }
			swal($translate.instant('Alertas.operacionCorrecta'));
            if($rootScope.horasAsignadasProyecto == true){
                swal($translate.instant('Alertas.orgCambios'));
            }
            $rootScope.horasAsignadasProyecto = false;
        });
    };

    var formar = function(idPoblacion, formacion, perfil){
        $scope.idPoblacion = idPoblacion;
        $scope.valor = formacion;
        $scope.perfil = perfil;

        if(!cambioFormacion) {
            return;
        }

        dataService.formar($rootScope.idEmpresaGeneral,$scope.idPoblacion,$scope.valor, $rootScope.momentoGeneral).then(function (result) {
            $scope.contratados($scope.perfil);
            $scope.formacion = $scope.valor;

            if (result.data.alertas == true) {
                dataService.getTotalAlertasNuevas($rootScope.idEmpresaGeneral).then(function (result) {
                    $rootScope.totalAlertas = result.data.datos;
                });
            }
            swal($translate.instant('Alertas.operacionCorrecta'));
            if($rootScope.horasAsignadasProyecto == true){
                swal($translate.instant('Alertas.orgCambios'));
            }
            $rootScope.horasAsignadasProyecto = false;
        });
    };

	
	var objetcToArray = function ($array, $object) {
		//reset
		$array = [];
		angular.forEach($object, function(element) {
			$array.push(element);
		});
		return $array;
	};	
	

}]);
