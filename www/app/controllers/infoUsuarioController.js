emprendeapp.controller('infoUsuarioController', ['$scope', 'dataService', '$rootScope','$location', function ($scope, dataService, $rootScope,$location) {
    $scope.usuario = $rootScope.loggedInUserName;
    $scope.pass = $rootScope.loggedInUserPass;

    $scope.aceptar = function(){
        $location.path("/menuPrincipal");
    }
}]);
