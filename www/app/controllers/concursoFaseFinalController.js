emprendeapp.controller('concursoFaseFinalController', ['$scope','dataService','$location','$rootScope','$translate', function ($scope,dataService,$location,$rootScope,$translate) {
	
	$scope.parseInt = parseInt;
	
	
	if ($rootScope.estadoFaseConcurso == null)
	{
		$scope.accesoFinal = false;
	}
	else
	{
		var estado = parseInt($rootScope.estadoFaseConcurso);
		console.log("estado fase " + estado);
		$scope.comenzado = ( estado == 2 );	
		$scope.accesoFinal = true;
	} 
	
	$scope.duracionDecision = 0;
	$scope.fase = null;

	console.log("accesofinal " + $scope.accesoFinal);
	
	$scope.cargarPartidaConcurso = function(){
		
		
		if ($scope.comenzado)
		{
			dataService.cargarPartida($rootScope.loggedInUserIdConcurso, $rootScope.tipoPartida).then(function (result){
				
				$rootScope.idEmpresaGeneral = result.data.idEmpresa;
				$rootScope.idMundoGeneral = result.data.idMundo;
				$rootScope.momentoGeneral = result.data.momento;
				
				window.localStorage.setItem("gameOverConcurso", false);
				window.localStorage.setItem("pasoenProgreso", false);
				window.localStorage.setItem("entroPorMenu", true);
				$rootScope.partidaTerminadaConcurso = window.localStorage.getItem("gameOverConcurso");
				$rootScope.gameOverConcurso = false;
				$rootScope.hayPartidaConcurso = true;
				
				$rootScope.showMenu = true;			
				$rootScope.isLogged = true;
				
				$location.path("/miEmpresa");
				
			});
		}
		else
		{
			//no se ha clasificado
			swal($translate.instant('Alertas.concursoFaseFinalNoClasificado'), "", "success");		
		}
	};	
	
	
	dataService.getFasesConcurso($rootScope.idConcurso).then(function(result){
		var longitud = result.data.datos.length;
		for(var i=0; i < longitud; i++){
			if (result.data.datos[i].tipoMundo == '4')
			{
				result.data.datos[i].duracionDecision = parseInt(result.data.datos[i].frecuencia) / 60;
				//base nombre imagen
				var icono = "FaseFinal_"; //1_GRIS
				//escenario
				var escenario = result.data.datos[i].idEscenario.substr(0, 1);
				icono += escenario;
				icono += "_";
				if (result.data.datos[i].estadoFase == '3' )
					icono += "ROJO";
				if ( (result.data.datos[i].estadoFase == '2' ) || (result.data.datos[i].estadoFase == '1' ) )
					icono += "VERDE";
				if (result.data.datos[i].estadoFase == '0' )
					icono += "GRIS";
				icono += "_icono";
				result.data.datos[i].icono = icono;
				
				$scope.fase = result.data.datos[i];
				//var fechaInicio = $scope.fase.fechaInicio.substring(0, 10);				
				var fechaInicio = $scope.fase.fechaInicioFaseFinal.substring(0, 10);
				console.log(fechaInicio.replace(/-/gi,''));
				$scope.fechaInicio = new Date(fechaInicio);
				
			}
		}
	});	

   
}]);
