emprendeapp.controller('pasoJornadaController', ['$rootScope','$scope', '$routeParams', '$location','$translate', function ( $rootScope, $scope, $routeParams,$location, $translate) {
	$scope.visible = true;
	$scope.estado = $routeParams.estado;
	
	window.localStorage.setItem("pasoenProgreso", true);
	
	if ($scope.estado == "inactiva")
	{
		$scope.visible = false;
	}
	
	$scope.hintSize = 21;
	$scope.pista = "";
	
	$scope.volver = function(){
		$location.path("/menuPrincipal");
    }
	
	$scope.finished = function(){
		//comprobar si paso jornada ok
		console.log( "finished: volvemos a preguntar al servidor ");
		$rootScope.getInfoMercadoMulti();
    }
	
	$scope.showRandomHint = function (timer) {
		var hintAleatorio = 'Hint.' + Math.floor((Math.random()* $scope.hintSize )+1);
		$scope.pista = $translate.instant(hintAleatorio);
		$("#sugerencias span.tip").html($scope.pista);
		//console.log( $scope.pista );
		if (timer)
			timer.start();
	}
	
	$scope.showRandomHint(null);
	$rootScope.showMenu = false;
}]);
