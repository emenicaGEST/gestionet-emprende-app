emprendeapp.controller('premiosController', ['$scope','dataService','$location','$rootScope','$translate', function ($scope,dataService,$location,$rootScope,$translate) {
  $scope.puntuacionUser = 0;
  
   dataService.getPremiosConcurso($rootScope.idConcurso).then(function (result){

   		$scope.premiosInfo = result.data.datos;

   });
	
    
    dataService.getSorteosConcurso($rootScope.idConcurso, $rootScope.loggedInUserIdConcurso).then(function (result){
    	$scope.sorteosInfo = result.data.datos;
		if (result.data != null && result.data.puntuaciones != null && result.data.puntuaciones.length > 0)
		{
			$scope.puntuacionUser = parseInt(result.data.puntuaciones[0].puntuacion);
			console.log(" $scope.puntuacionUser " + $scope.puntuacionUser);
		}
    });
	
}]);
