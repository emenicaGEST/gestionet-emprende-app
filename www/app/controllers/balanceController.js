emprendeapp.controller('balanceController', ['$scope', '$routeParams', 'dataService','$location','$rootScope', function ($scope, $routeParams, dataService,$location,$rootScope) {
    $rootScope.isLogged = true;
    $scope.balanceInfo;
    $scope.oneAtATime = true;
	$scope.momentoVisualizado = $rootScope.momentoGeneral;
	$scope.mesVisualizada = $rootScope.mesActual;
	$scope.dataBwdAvailable = false;
	$scope.dataFwdAvailable = false;
	
    $scope.pantallaInformacion = function(){
        $location.path('informacion/pantalla/nombreBalance/parrafo_unoBalance/parrafo_dosBalance');
    };
	
	$scope.historico = function(direccion){
		//console.log("Historico: " + direccion);
        $("#chart").remove();
        $(".capaCanvas").append("<canvas id='chart' width='100' height='100'></canvas>");
        $("#chart1").remove();
        $(".capaCanvas1").append("<canvas id='chart1' width='100' height='100'></canvas>");
		//adecuamos la jornada a visualizar
		$scope.momentoVisualizado = parseInt($scope.momentoVisualizado) + parseInt(direccion);
		//console.log("momento visualizado: " + $scope.momentoVisualizado);		
		if ($scope.momentoVisualizado < $rootScope.momentoGeneral)
		{
			$scope.dataFwdAvailable = true;
		}
		else
		{
			$scope.momentoVisualizado = $rootScope.momentoGeneral;
			$scope.dataFwdAvailable = false;
		}
			
		if  ($scope.momentoVisualizado > 0)
		{
			$scope.dataBwdAvailable = true;
		}
		else
		{
			$scope.dataBwdAvailable = false;
			$scope.momentoVisualizado = 0;
		}
		//console.log("jornada: " + $rootScope.momentoGeneral);
		//console.log("momento visualizado adecuado: " + $scope.momentoVisualizado);
		//console.log("jornadas atras: " + $scope.dataBwdAvailable);
		//console.log("jornadas adelante: " + $scope.dataFwdAvailable);
		//console.log("tipo partida: " + $rootScope.tipoPartida);
		//actualizamos fecha visualizada
		if ($rootScope.tipoPartida == 1)
		{
			$scope.mesVisualizado = $rootScope.devolverMes($scope.momentoVisualizado);
		}
		else
		{
			$scope.momentoVisualizadoMundo = $rootScope.mundoMultiData.mundo.jornadaActual - $rootScope.momentoGeneral + parseInt($scope.momentoVisualizado);
			$scope.mesVisualizado = $rootScope.devolverMes($scope.momentoVisualizadoMundo);
		}
		
		
		
		//llamamos al servidor para obtener los datos de esa jornada
		dataService.getBalance($rootScope.idEmpresaGeneral, $rootScope.idMundoGeneral, $scope.momentoVisualizado-1).then($scope.showData);
	};
    //dataService.getBalance($rootScope.idEmpresaGeneral, $rootScope.idMundoGeneral, $rootScope.momentoGeneral).then(function (result){	
	$scope.showData = function (result){
        $scope.balanceInfo = result.data;
		console.log("balance info: ");
		console.log($scope.balanceInfo);
        //Chart1
        var ctx = document.getElementById("chart").getContext("2d");

        var data = [
            {
                value: parseInt($scope.balanceInfo.activoCorrienteGrafica),
                color:"#889641",
                label: "Activo corriente"
            },
            {
                value: parseInt($scope.balanceInfo.activoNoCorrienteGrafica),
                color: "#CED2AD",
                label: "Activo no corriente"
            }
        ]

        new Chart(ctx).Pie(data, {
            showTooltips: false
        });


        //Chart2
        var ctx1 = document.getElementById("chart1").getContext("2d");
		
		var dataGraficaPatNeto = $scope.balanceInfo.PATRIMONIONETOGRAFICA;
		if (dataGraficaPatNeto < 0)
			dataGraficaPatNeto = 0;
		var dataGraficaPasCor = $scope.balanceInfo.PASIVOCORRIENTEGRAFICA;
		if (dataGraficaPasCor < 0)
			dataGraficaPasCor = 0;
		var dataGraficaPasNoCor = $scope.balanceInfo.PASIVONOCORRIENTEGRAFICA;
		if (dataGraficaPasNoCor < 0)
			dataGraficaPasNoCor = 0;
		
        var data = [
            {
                value: parseInt(dataGraficaPatNeto),
                color:"#545454",
                label: "Patrimonio neto"
            },
            {
                value: parseInt(dataGraficaPasCor),
                color: "#E5E5E5",
                label: "Pasivo corriente"
            },
            {
                value: parseInt(dataGraficaPasNoCor),
                color: "#B1B1B1",
                label: "Pasivo no corriente"
            }
        ]

        new Chart(ctx1).Pie(data, {
            showTooltips: false
        });

    };

	//inicializar
	$scope.historico(0);

}]);
