emprendeapp.controller('subvencionesController', ['$scope', 'dataService','$location','$rootScope','$translate', function ($scope, dataService,$location,$rootScope,$translate) {
    $scope.idSubvencion;
    $scope.SubvencionInfoObject;
	$scope.SubvencionInfo;
    $scope.SubvencionSolicitadaInfo;
	
	$scope.momentoBase = 0;

    $scope.fechaPublicada = [];
    for (var i = 0; i < 24; i++) {
        $scope.fechaPublicada.push($translate.instant('Subvenciones.FechaConAnio.' + i))
    };

    dataService.obtenerSubvencionesDisponibles($rootScope.idEmpresaGeneral, $rootScope.momentoGeneral).then(function(result) {
        $scope.SubvencionInfoObject = result.data.datos;
		/* fix object -> array */
		$scope.SubvencionInfo = objetcToArray($scope.SubvencionInfo, $scope.SubvencionInfoObject);
		
        $scope.ningunaSubvencion = result.data.datos.length == 0;

    });
    $scope.pantallaInformacion = function(){
        $location.path('informacion/pantalla/nombreSub/parrafo_unoSub/parrafo_dosSub');
    };
    $scope.obtenerSubvencionesSolicitadas = function (){
        dataService.obtenerSubvencionesSolicitadas($rootScope.idEmpresaGeneral, $rootScope.momentoGeneral).then(function(result){
			
			if ($rootScope.tipoPartida != 1 && $rootScope.mundoMultiData != null)
			{
				$scope.momentoBase = $rootScope.mundoMultiData.mundo.jornadaActual - $rootScope.momentoGeneral;
			}
			//console.log( "momento base " + $scope.momentoBase );
            $scope.SubvencionSolicitadaInfo = result.data.datos;
            $scope.ningunaSubvencionSolicitada = result.data.datos.length == 0;
        });
    };


    $scope.solicitarSubvencion = function (subvencion){
        $scope.idSubvencion = subvencion.idSubvencion;
        dataService.solicitarSubvencion($rootScope.idEmpresaGeneral, $scope.idSubvencion).then(function(result) {
            //actualizar lsita de subvenciones solicitadas con la nueva
			$scope.obtenerSubvencionesSolicitadas();
			//eliminar la subvención de la lista de disponibles
			console.log($scope.SubvencionInfo);
			var indexToDelete = indexFromUser($scope.SubvencionInfo, $scope.idSubvencion);
			console.log($scope.SubvencionInfo);
			console.log(typeof $scope.SubvencionInfo);
			
			
			$scope.SubvencionInfo.splice(indexToDelete, 1);
			//avisar al usuario
            swal($translate.instant('Alertas.operacionCorrecta'));

        });
    };
    $scope.obtenerSubvencionesSolicitadas();

	
	var indexFromUser = function (array, id) {
        for (var i = 0; i < array.length; i++) {
            if (array[i].idPoblacion == id) {
                return i;
            }
        }
        return false;
    }

	var objetcToArray = function ($array, $object) {
		//reset
		$array = [];
		angular.forEach($object, function(element) {
			$array.push(element);
		});
		return $array;
	};

}]);
