emprendeapp.controller('rankingController', ['$scope', 'dataService', '$rootScope','$location','$translate', function ($scope, dataService, $rootScope,$location,$translate) {
    $scope.showRanking = true;
     // 2 ranking personal
    $scope.NoHayDatos = false;

    dataService.getRankingIndividual($rootScope.loggedInUserId, 20, 2, 1).then(function(result) {
        $scope.ranking = result.data.datos;
        //convertirAFecha();
        convertirAEntero();
    });


    //RANKING_GENERAL_TOP= 0
    dataService.getRankingIndividual($rootScope.loggedInUserId, 5, 0, 1).then(function(result) {
        $scope.rankingGeneralTop = result.data.datos;
        //convertirAFecha();
        convertirAEntero();
    });

    //RANKING_GENERAL_USUARIO=1
    dataService.getRankingIndividual($rootScope.loggedInUserId, 4, 1, 1).then(function(result) {
        $scope.rankingGeneralUsuario = result.data.datos.listado;
        $scope.posicion = result.data.datos.posicionUsuario;
        $scope.mostrar = true;
        if($scope.posicion <= 5){
            $scope.mostrar = false;
        }

        //convertirAFecha();
        convertirAEntero();
    });


    var convertirAFecha = function() {
        angular.forEach($scope.ranking, function(fila) {
            fila.fechaInicio = new Date(fila.fechaInicio);			
        })
    };
	var convertirAEntero = function() {
        angular.forEach($scope.ranking, function(fila) {
			fila.puntuacion = parseInt(fila.puntuacion);
        })
    };

    $scope.seleccionarSitDif = function(situacion, dificultad){
        $scope.NoHayDatos = false;
        dataService.getRanking($rootScope.loggedInUserId, 20, 2, situacion, dificultad, 1).then(function(result) {

            $scope.ranking = result.data.datos;
            if ($scope.ranking == null || $scope.ranking == ''){
                $scope.NoHayDatos = true;
                $scope.Datos = $translate.instant('Ranking.noDatos');
            }else{

            }
            //convertirAFecha();
            convertirAEntero();
        });

        //RANKING_GENERAL_TOP= 0
        dataService.getRanking($rootScope.loggedInUserId, 5, 0, situacion, dificultad, 1).then(function(result) {
            $scope.rankingGeneralTop = result.data.datos;
            if ($scope.rankingGeneralTop == null || $scope.rankingGeneralTop == ''){
                $scope.NoHayDatos = true;
                $scope.Datos = $translate.instant('Ranking.noDatos');
            }else{

            }
            //convertirAFecha();
            convertirAEntero();
        });

        //RANKING_GENERAL_USUARIO=1
        dataService.getRanking($rootScope.loggedInUserId, 4, 1, situacion, dificultad, 1).then(function(result) {


            if (result.data.datos == null || result.data.datos == ''){
                $scope.NoHayDatos = true;
                $scope.Datos = $translate.instant('Ranking.noDatos');
                $scope.mostrar = false;
            }else{
                $scope.rankingGeneralUsuario = result.data.datos.listado;
                $scope.posicion = result.data.datos.posicionUsuario;
                $scope.mostrar = true;
                if($scope.posicion <= 5){
                    $scope.mostrar = false;
                }
            }

            //convertirAFecha();
            convertirAEntero();
        });
    }


}]);
