emprendeapp.controller('concursoLigaNacionalController', ['$scope','dataService','$location','$rootScope','$translate', function ($scope,dataService,$location,$rootScope,$translate) {
	
	$scope.parseInt = parseInt;
	var estado = parseInt($rootScope.estadoFaseConcurso);
	$scope.comenzado = ( estado == 2 );
	console.log("estado " + estado);
	$scope.anchoFase = 100;
	$scope.fasesInfo = new Array();
	$scope.duracionDecision = 0;
	
	$scope.updateFrecuenciaDecision = function( index ) {
		//console.log("updateFrecuenciaDecision " + index + " --> " + $scope.fasesInfo[index].duracionDecision );
		if ($scope.fasesInfo.length > index)
			$scope.duracionDecision = $scope.fasesInfo[index].duracionDecision;
	}
	
	$scope.cargarPartidaConcurso = function(){
		
		dataService.cargarPartida($rootScope.loggedInUserIdConcurso, $rootScope.tipoPartida).then(function (result){
			
			
			$rootScope.idEmpresaGeneral = result.data.idEmpresa;
			$rootScope.idMundoGeneral = result.data.idMundo;
			$rootScope.momentoGeneral = result.data.momento;
			
			window.localStorage.setItem("gameOverConcurso", false);
			window.localStorage.setItem("pasoenProgreso", false);
			window.localStorage.setItem("entroPorMenu", true);
			$rootScope.partidaTerminadaConcurso = window.localStorage.getItem("gameOverConcurso");
			$rootScope.gameOverConcurso = false;
			$rootScope.hayPartidaConcurso = true;
			
			$rootScope.showMenu = true;			
			$rootScope.isLogged = true;
			
			//$location.path("/miEmpresa");
			$rootScope.getInfoMercadoMulti();
			
		});

	};	
	
	
	dataService.getFasesConcurso($rootScope.idConcurso).then(function(result){
		var longitud = result.data.datos.length;
		for(var i=0; i < longitud; i++){
			if (result.data.datos[i].tipoMundo == '3')
			{
				result.data.datos[i].duracionDecision = parseInt(result.data.datos[i].frecuencia) / 60;
				//base nombre imagen
				var icono = "LigaNacional_icon"; //1_GRIS
				//escenario
				var escenario = result.data.datos[i].idEscenario.substr(0, 1);
				icono += escenario;
				icono += "_";
				if (result.data.datos[i].estadoFase == '3' )
					icono += "ROJO";
				if (result.data.datos[i].estadoFase == '2' ) 
					icono += "VERDE";
				if ( (result.data.datos[i].estadoFase == '0') || (result.data.datos[i].estadoFase == '1' ))
					icono += "GRIS";
				
				result.data.datos[i].icono = icono;
				//result.data.datos[i].mesesEscenario = (parseInt(escenario)-1) * 6;
				if (escenario == 1)
					result.data.datos[i].mesesEscenario = "Partida.creacion";
				else if (escenario == 2)
					result.data.datos[i].mesesEscenario = "Partida.seismesesCrea";
				else if (escenario == 3)
					result.data.datos[i].mesesEscenario = "Partida.docemesesCrea";
					
				$scope.fasesInfo.push(result.data.datos[i]);
				
				//estilo botones fases
				$scope.anchoFase = $scope.anchoFase / $scope.fasesInfo.length;
				
				//inicializar duracion
				//console.log("fase estado " + result.data.datos[i].estado);
				if ( (result.data.datos[i].estadoFase == '1' ) || (result.data.datos[i].estadoFase == '2' ) )
					$scope.updateFrecuenciaDecision($scope.fasesInfo.length-1);
				
			}
		}
	});	

   
}]);
