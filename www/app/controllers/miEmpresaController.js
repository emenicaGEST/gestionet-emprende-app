emprendeapp.controller('miEmpresaController', ['$scope', '$routeParams', 'dataService','$location','$rootScope','$translate', '$filter', function ($scope, $routeParams, dataService,$location,$rootScope,$translate, $filter){
    $scope.idEmpresa = $routeParams.idEmpresa;
    $scope.momento;
    $scope.valoracionGlob;
    $scope.valoracionGlobAvg;
    $rootScope.showMenu = true;
    $scope.inversionesfin;
    $scope.infoMercado;
    $scope.rrhh;
    $scope.gestProyectos;
	$scope.entorno = dataService.getEntornoDespliegue();
	
	$scope.ifFacil_patrimonioInicial = 15000;
	$scope.if6mesesFacil_prestamoPedido = 11000;
	$scope.if6mesesFacil_descubierto = 13000;	
	$scope.if6mesesMedio_fondoInversion = 2000;
	$scope.if6mesesMedio_descubierto = 13000;
	$scope.if6mesesDificil_fondoInversion = 2000;
	$scope.if6mesesDificil_descubierto = 10000;
	$scope.infGestProy6mesesFacil_proyectosCifraMin = 9000;
	$scope.infGestProy6mesesFacil_proyectosCifraMax = 9500;
	$scope.infGestProy12mesesFacil_proyectosCifra = 9100;
	
	
    $scope.pantallaInformacion = function(){
        $location.path('informacion/pantalla/nombre/parrafo_uno/parrafo_dos');
    };

    $rootScope.get_MomentoActualApp = function(){
        dataService.get_MomentoActualApp($rootScope.idEmpresaGeneral).then(function (result){
            $scope.momento = result.data.momento;
            $rootScope.momentoGeneral = result.data.momento;
            $rootScope.idEscenario = result.data.idEscenario;
            //console.log("get_MomentoActualApp momentoGeneral " +  $rootScope.momentoGeneral);
			//actualizar cambios de moneda con datos del servidor
			$rootScope.cambiosMoneda = result.data.cambiosMoneda;
			
            valoraciones($rootScope.momentoGeneral);

			if ($rootScope.tipoPartida == 1)
			{
				$rootScope.mesVisualizadoGeneral = $rootScope.devolverMes($rootScope.momentoGeneral);
			}
			else
			{
				$scope.momentoVisualizadoMundo = $rootScope.mundoMultiData.mundo.jornadaActual;
				$rootScope.mesVisualizadoGeneral = $rootScope.devolverMes($scope.momentoVisualizadoMundo);
			}
			
            dataService.getTotalAlertasNuevas($rootScope.idEmpresaGeneral).then(function (result) {
                $rootScope.totalAlertas = result.data.datos;
            });
            $scope.inversionesfin="";
            $scope.infoMercado="";
            $scope.rrhh="";
            $scope.gestProyectos="";
            if($rootScope.idEscenario == '21') {
                //6 meses
                $scope.inversionesfin =$translate.instant('Empresa.if6mesesFacil_Uno') + $filter('mycurrency')($rootScope.convertirMonedaBaseActual($scope.if6mesesFacil_prestamoPedido)) + $translate.instant('Empresa.if6mesesFacil_Dos')
					+ $filter('mycurrency')($rootScope.convertirMonedaBaseActual($scope.if6mesesFacil_descubierto)) + $translate.instant('Empresa.if6mesesFacil_Tres');
                $scope.infoMercado =$translate.instant('Empresa.infMercado6mesesFacil');
                $scope.rrhh=$translate.instant('Empresa.infRrhh6mesesFacil');
                $scope.gestProyectos =$translate.instant('Empresa.infGestProy6mesesFacil_Uno') + $filter('mycurrency')($rootScope.convertirMonedaBaseActual($scope.infGestProy6mesesFacil_proyectosCifraMin)) + $translate.instant('Empresa.infGestProy6mesesFacil_Dos')
					 + $filter('mycurrency')($rootScope.convertirMonedaBaseActual($scope.infGestProy6mesesFacil_proyectosCifraMax)) + $translate.instant('Empresa.infGestProy6mesesFacil_Tres');

            }else if($rootScope.idEscenario == '22'){

                $scope.inversionesfin =$translate.instant('Empresa.if6mesesMedio_Uno') + $filter('mycurrency')($rootScope.convertirMonedaBaseActual($scope.if6mesesMedio_fondoInversion)) + $translate.instant('Empresa.if6mesesMedio_Dos')
					+ $filter('mycurrency')($rootScope.convertirMonedaBaseActual($scope.if6mesesMedio_descubierto)) + $translate.instant('Empresa.if6mesesMedio_Tres');
                $scope.infoMercado =$translate.instant('Empresa.infMercado6mesesMedio');
                $scope.rrhh=$translate.instant('Empresa.infRrhh6mesesMedio');
                $scope.gestProyectos =$translate.instant('Empresa.infGestProy6mesesMedio');
            }else if($rootScope.idEscenario == '23'){

                $scope.inversionesfin =$translate.instant('Empresa.if6mesesDificil_Uno') + $filter('mycurrency')($rootScope.convertirMonedaBaseActual($scope.if6mesesDificil_descubierto)) + $translate.instant('Empresa.if6mesesDificil_Dos')
					+ $filter('mycurrency')($rootScope.convertirMonedaBaseActual($scope.if6mesesDificil_fondoInversion)) + $translate.instant('Empresa.if6mesesDificil_Tres');
                $scope.infoMercado =$translate.instant('Empresa.infMercado6mesesDificil');
                $scope.rrhh=$translate.instant('Empresa.infRrhh6mesesDificil');
                $scope.gestProyectos =$translate.instant('Empresa.infGestProy6mesesDificil');

            }else if($rootScope.idEscenario == '31'){

                $scope.inversionesfin =$translate.instant('Empresa.if12mesesFacil');
                $scope.infoMercado =$translate.instant('Empresa.infMercado12mesesFacil');
                $scope.rrhh=$translate.instant('Empresa.infRrhh12mesesFacil');
                $scope.gestProyectos =$translate.instant('Empresa.infGestProy12mesesFacil_Uno');// + $filter('mycurrency')($rootScope.convertirMonedaBaseActual($scope.infGestProy12mesesFacil_proyectosCifra)) + $translate.instant('Empresa.infGestProy12mesesFacil_Dos');
            }else if($rootScope.idEscenario == '32'){

                $scope.inversionesfin =$translate.instant('Empresa.if12mesesMedio');
                $scope.infoMercado =$translate.instant('Empresa.infMercado12mesesMedio');
                $scope.rrhh=$translate.instant('Empresa.infRrhh12mesesMedio');
                $scope.gestProyectos =$translate.instant('Empresa.infGestProy12mesesMedio');
            }else if($rootScope.idEscenario == '33'){

                $scope.inversionesfin =$translate.instant('Empresa.if12mesesDificil');
                $scope.infoMercado =$translate.instant('Empresa.infMercado12mesesDificil');
                $scope.rrhh=$translate.instant('Empresa.infRrhh12mesesDificil');
                $scope.gestProyectos =$translate.instant('Empresa.infGestProy12mesesDificil');
            }else{

                $scope.inversionesfin = $translate.instant('Empresa.ifFacil_Uno'); // + $filter('mycurrency')($rootScope.convertirMonedaBaseActual($scope.ifFacil_patrimonioInicial)) + $translate.instant('Empresa.ifFacil_Dos');
                $scope.infoMercado = $translate.instant('Empresa.infMercado');
                $scope.rrhh=$translate.instant('Empresa.infRrhh');
                $scope.gestProyectos =$translate.instant('Empresa.infGestProy');
            }
        });
    };


    var devolverMesGrafica = function(momento, idEscenario){
        var meses = [""];
        var j;
        if(idEscenario == '21' || idEscenario == '22' || idEscenario == '23') {
            j = 5;
            for(var i=4; i<momento; i++ ) {
                 //fecha = $translate.instant('Fechas.Mes'+momento);
                fecha = $rootScope.devolverMes(j);
                meses.push(fecha);

               /* if(fecha == "Diciembre") {
                    j = 1;
                } else {
                    j++;
                }*/
                j++;
            }
        }
        else if($rootScope.idEscenario == '31' || $rootScope.idEscenario == '32' || $rootScope.idEscenario == '33'){
            j = 11;
            for(var i=10; i<momento; i++ ) {
                //fecha = $translate.instant('Fechas.Mes'+momento);
                fecha = $rootScope.devolverMes(j);
                meses.push(fecha);

               /* if(fecha == "Diciembre") {
                    j = 1;
                } else {
                    j++;
                }*/
                j++;
            }
        }
        else {
            j = 0;
            for(var i=-1; i<momento; i++ ) {
				mes = j;
				if ( ($rootScope.tipoPartida == 2) || ($rootScope.tipoPartida == 3) )
				{
					mes = $rootScope.mundoMultiData.mundo.jornadaActual - $rootScope.momentoGeneral + j;					
				}
                //fecha = $translate.instant('Fechas.Mes'+momento);
				fecha = $rootScope.devolverMes(mes);
				//console.log(mes);
                meses.push(fecha);

                /*if(fecha == "Diciembre") {
                    j = 1;
                } else {
                    j++;
                }*/
                j++;
            }
        }

        return meses;
    };
	$rootScope.devolverMesIndice = function(momento){
        //$rootScope.mesActual= "";
		//obtener el año y mes correspondientes
		var anio = Math.floor(momento/12);
		var mes = momento % 12;
		mes++;
		return mes;
	}
	
    $rootScope.devolverMes = function(momento){
        //$rootScope.mesActual= "";
		//obtener el año y mes correspondientes
		var anio = Math.floor(momento/12);
		var mes = momento % 12;
		//ajustar
		anio++;
		mes++;
		//debug
		//console.log("Año:" + anio);
		//console.log("mes:" + mes);
        //var mesesAnio = ["Ene - Año 1", "Feb - Año 1", "Mar - Año 1", "Abr - Año 1", "May - Año 1", "Jun - Año 1","Jul - Año 1", "Ago - Año 1", "Sep - Año 1", "Oct - Año 1", "Nov - Año 1", "Dic - Año 1", "Ene - Año 2", "Feb - Año 2", "Mar - Año 2"];
        //$rootScope.mesActual = mesesAnio[momentoGeneral];
		//$rootScope.mesActual = $translate.instant('Fechas.Mes'+mes) + ' - ' + $translate.instant('Fechas.Anio') + ' ' + anio;
		fecha = $translate.instant('Fechas.Mes'+mes) + ' - ' + $translate.instant('Fechas.Anio') + ' ' + anio;
        //console.log(fecha);
		return fecha;
        //return mesActual[momentoGeneral];
    };

    var valoraciones = function (momento) {
        //console.log("Valor del momento: " + momento);
       // $scope.momento = momento;
        dataService.valoraciones($rootScope.idEmpresaGeneral, $rootScope.momentoGeneral).then(function (result){
            if (result.data.datos.actuales.length == 0){
                $scope.valoracionGlob = 0;
                $scope.valoracionGlobAvg = 0;
            }else if (result.data.datos.actuales.length != 0){
                $scope.valoracionGlob = result.data.datos.actuales.valglobal;
                $scope.valoracionGlobAvg = result.data.datos.actuales.media;
            }

            var cargarGrafica = function(){
                var momentosPasados = [];
                var generateData = function() {
                    var data = [];
                    $scope.momentoActual = parseInt($rootScope.momentoGeneral) - 1;
                    var i;
                    if($rootScope.idEscenario == '21' || $rootScope.idEscenario == '22' || $rootScope.idEscenario == '23'){
                        i=5;
                    }else if($rootScope.idEscenario == '31' || $rootScope.idEscenario == '32' || $rootScope.idEscenario == '33'){
                        i=11;
                    }
                    else{
                        i=0;
                    }
                    for(i; i <= $scope.momentoActual; ++i){
                        momentosPasados = parseFloat(result.data.datos.historicoValGloblal[i]);
                        data.push(momentosPasados);
                    }
                    data.unshift(0);
                    return data;
                };

                var momentosPasadosVM = [];
                var generateDataVM = function() {
                    var data = [];
                    $scope.momentoActual = parseInt($rootScope.momentoGeneral) - 1;
                    var i;
                    if($rootScope.idEscenario == '21' || $rootScope.idEscenario == '22' || $rootScope.idEscenario == '23'){
                        i=5;
                    }else if($rootScope.idEscenario == '31' || $rootScope.idEscenario == '32' || $rootScope.idEscenario == '33'){
                        i=11;
                    }
                    else{
                        i=0;
                    }
                    for(i; i <= $scope.momentoActual; ++i) {
                        momentosPasadosVM = parseFloat(result.data.datos.historicoMedia[i]);
                        data.push(momentosPasadosVM);
                        //console.log("data dentro if generateDataVM " + data);
                    }
                    //console.log(data);
                    data.unshift(0);
                    return data;
                };

                var ctx = document.getElementById("chartLine").getContext("2d");
                var data = {
                    labels: devolverMesGrafica($rootScope.momentoGeneral, $rootScope.idEscenario),					
                    datasets: [
                        {
                            label: "Valoración global",
                            fillColor: "rgba(27, 153, 255,0)",
                            strokeColor: "#2B7C96",
                            pointColor: "#2B7C96",
                            pointStrokeColor: "#2B7C96",
                            pointHighlightFill: "#2B7C96",
                            pointHighlightStroke: "#2B7C96",
                            data: generateData()
                        },
                        {
                            label: "Valoración media",
                            fillColor: "rgba(3,151,20,0)",
                            strokeColor: "#D0A426",
                            pointColor: "#D0A426",
                            pointStrokeColor: "#D0A426",
                            pointHighlightFill: "#D0A426",
                            pointHighlightStroke: "#D0A426",
                            data: generateDataVM()
                        }
                    ]
                };
                new Chart(ctx).Line(data, {
                    showTooltips: true,
                    animation: true,
                    responsive: true
                });
            }
            $("#chartLine").remove();
            $(".capaCanvasInv").append("<canvas id='chartLine'></canvas>");
            cargarGrafica();

        });
    };

    $rootScope.get_MomentoActualApp();
    $scope.oneAtATime = true;


}]);
