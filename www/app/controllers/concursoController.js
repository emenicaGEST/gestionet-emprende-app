emprendeapp.controller('concursoController', ['$scope','dataService','$location','$rootScope', '$translate', function ($scope,dataService,$location,$rootScope,$translate) {
	$rootScope.tituloEmprende = false;
	$rootScope.tituloEmprendeConcurso = true;
	$scope.rutaFase;
	$scope.accesible = false;
	$scope.registrado = false;
	$scope.reserva = false;
	
	$scope.fases =[];
	//$rootScope.loggedInUserIdConcurso = window.localStorage.getItem("loggedInUserIdConcurso")
	
	$rootScope.volverEmprende = function(){
		$rootScope.tituloEmprende = true;
		$rootScope.tituloEmprendeConcurso = false;
	}
	
	$scope.epocaConcursoFase = function(){
		console.log("reserva " + $scope.reserva);
		if ($scope.reserva == 1)
		{
			//reserva
			swal($translate.instant('Alertas.concursoInscripcionMaxParticipantesAlcanzado'), "", "success");		
		}
		else
		{
			$location.path($scope.rutaFase);
		}
	}
	
	$scope.navegarBasesCalendario =  function () {
		if($rootScope.dentroEpocaConcurso == true){
			$location.path("/calendario");
		}
		else
		{
			$location.path("/bases");
		}
	}

	var epocaConcurso = function(){
		if($rootScope.dentroEpocaConcurso == true)
		{
			if (!$scope.registrado)
			{
				$location.path("/login");
			}
			else
			{
				dataService.getFasesConcurso($rootScope.idConcurso).then(function(result){
					$scope.accesible = false;
					/*var longitud = result.data.datos.length;					
					for(var i=0; i < longitud; i++){					
						//fase final
						if (result.data.datos[i].tipoMundo == '4' && (result.data.datos[i].estadoFase == '1' || result.data.datos[i].estadoFase == '2')){
							$scope.rutaFase = "/concursoFaseFinal";
							$scope.accesible = true;
							$rootScope.tipoPartida = 4;
							//liga nacional
						}else if(result.data.datos[i].tipoMundo == '3' && (result.data.datos[i].estadoFase == '1' || result.data.datos[i].estadoFase == '2')){
							$scope.rutaFase = "/concursoLigaNacional";
							$scope.accesible = true;
							$rootScope.tipoPartida = 3;
						}
					}*/
					angular.forEach(result.data.datos, function(fase) {
						//fase final
						if (fase.tipoMundo == '4' && (fase.estadoFase == '1' || fase.estadoFase == '2')){
							$scope.rutaFase = "/concursoFaseFinal";
							$scope.accesible = true;
							$rootScope.tipoPartida = 4;
							//liga nacional
						}else if(fase.tipoMundo == '3' && (fase.estadoFase == '1' || fase.estadoFase == '2')){
							$scope.rutaFase = "/concursoLigaNacional";
							$scope.accesible = true;
							$rootScope.tipoPartida = 3;
						}
					});
				});	
			}
		}
		
		if($rootScope.dentroEpocaRegistro == true){
			
			$scope.rutaFase = "/registro";
		}
		
		//$rootScope.tipoPartida = 3;
	
	}


	var getEmpresaUserConcurso = function(){
		
		dataService.getEmpresaUserConcurso($rootScope.idConcurso, $rootScope.loggedInUserIdConcurso).then(function (result){
			console.log( "test " + result.data.status  );				
			$scope.fasesInfo = result.data.datos;
			$scope.fases = objetcToArray($scope.fases, $scope.fasesInfo);
			
			if (result.data.status == "success" && $scope.fases.length > 0)
			{
				for (var i=0; i < $scope.fases.length; i++)
				{
					console.log( "mundobase " + $scope.fases[i].idConcursoMundoBase + " estado " + $scope.fases[i].estado + " Estado Fase " + $scope.fases[i].estadoFase );
					$rootScope.idUsuarioConcurso = $scope.fases[i].idUsuario;
					if ( $scope.fases[i].Estado == 0 || $scope.fases[i].Estado == 1)					
					{
						$rootScope.idEmpresaGeneral = $scope.fases[i].idEmpresa;
						//$rootScope.idUsuarioConcurso = result.data.datos[i].idUsuario;
						$rootScope.idConcursoMundoBase = $scope.fases[i].idConcursoMundoBase;	
						$rootScope.estadoFaseConcurso = $scope.fases[i].estadoFase;
						if ( $scope.fases[i].estadoFase != 3)
							break;
					}
				}
			}
		});
	};	
	
	var checkUserRegistrado = function(){
		dataService.getUserConcurso($rootScope.idConcurso, $rootScope.loggedInUserIdConcurso).then(function (result){
			
			if (result.data.status == "success")
			{
				$scope.registrado = true;
				$scope.reserva = result.data.datos[0]['reserva'];
				console.log("reserva " + $scope.reserva);
				if ( $rootScope.dentroEpocaConcurso == true)
				{
					getEmpresaUserConcurso();
				}
			}
			else
			{
				$scope.registrado = false;
				//
			}
			epocaConcurso();
		});
	};

	
	checkUserRegistrado();	
	
	var objetcToArray = function ($array, $object) {
		//reset
		$array = [];
		angular.forEach($object, function(element) {
			$array.push(element);
		});
		return $array;
	};
	
}]);
