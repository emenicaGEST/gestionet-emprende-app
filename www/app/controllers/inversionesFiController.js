emprendeapp.controller('inversionesFiController', ['$scope', '$routeParams', 'dataService','$location','$rootScope','$translate', function ($scope, $routeParams, dataService,$location,$rootScope,$translate) {
    $scope.showLeyendas = false;

    $scope.cantidadDisponible;
    $scope.fondosInfo;
    $scope.idTipoFondosInversion;
    $scope.comp;
    $scope.vend;
	$scope.compConvertido;
    $scope.vendConvertido;
    $scope.momentoLength;

    $scope.nombresRenta = [$translate.instant('InversionesFinancieras.rentaFija'), $translate.instant('InversionesFinancieras.rentaMixta'),$translate.instant('InversionesFinancieras.rentaVariable') ];
    $scope.pantallaInformacion = function(){
        $location.path('informacionIf/');
    };
    dataService.getFondosInversion($rootScope.idEmpresaGeneral, $rootScope.idMundoGeneral, $rootScope.momentoGeneral).then(function (result){
        $scope.fondosInfo = result.data.datos;
        $scope.showLeyendas = true;
        var cargarGrafica = function(){
            var momentosPasados = [];
            var generateDataRF = function() {
                var data = [];
				var i;
				var margen = 0;
				if ($rootScope.tipoPartida != 1)
				{
					//adecuar inicio de datos de tdi para ajustar la gr�fica al momento correcto del mundo
					margen = $rootScope.mundoMultiData.mundo.jornadaActual - $rootScope.momentoGeneral;
				}
                for(i = -1; i <= $rootScope.momentoGeneral; ++i) {
                    momentosPasados = parseFloat(result.data.datos.tiposAnuales.Fija[margen+i]);
                    data.push(momentosPasados);
                    //console.log("data dentro if Fija" + data);
                }
                //console.log(data);
                return data;
            };

            var momentosPasadosRM = [];
            var generateDataRM = function() {
                var data = [];
				var i;
				var margen = 0;
				if ($rootScope.tipoPartida != 1)
				{
					//adecuar inicio de datos de tdi para ajustar la gr�fica al momento correcto del mundo
					margen = $rootScope.mundoMultiData.mundo.jornadaActual - $rootScope.momentoGeneral;
				}
                for(i = -1; i <= $rootScope.momentoGeneral; ++i) {
                    momentosPasadosRM = parseFloat(result.data.datos.tiposAnuales.Mixta[margen+i]);
                    data.push(momentosPasadosRM);
                    //console.log("data dentro if Mixta" + data);
                }
                //console.log(data);
                return data;
            };

            var momentosPasadosRV = [];
            var generateDataRV = function() {
                var data = [];
				var i;
				var margen = 0;
				if ($rootScope.tipoPartida != 1)
				{
					//adecuar inicio de datos de tdi para ajustar la gr�fica al momento correcto del mundo
					margen = $rootScope.mundoMultiData.mundo.jornadaActual - $rootScope.momentoGeneral;
				}
                for(i = -1; i <= $rootScope.momentoGeneral; ++i) {
                    momentosPasadosRV = parseFloat(result.data.datos.tiposAnuales.Variable[margen+i]);
                    data.push(momentosPasadosRV);
                    //console.log("data dentro if Variable" + data);
                }
                //console.log(data);
                return data;
            };

            var ctx = document.getElementById("chartLine").getContext("2d");
            var data = {
                labels: $rootScope.devolverMesGrafica($rootScope.momentoGeneral, $rootScope.idEscenario),
                datasets: [
                    {
                        label: "renta fija",
                        fillColor: "rgba(27, 153, 255,0)",
                        strokeColor: "#2B7C96",
                        pointColor: "#2B7C96",
                        pointStrokeColor: "#2B7C96",
                        pointHighlightFill: "#2B7C96",
                        pointHighlightStroke: "#2B7C96",
                        data: generateDataRF()
                    },
                    {
                        label: "mixta",
                        fillColor: "rgba(3,151,20,0)",
                        strokeColor: "#D0A426",
                        pointColor: "#D0A426",
                        pointStrokeColor: "#D0A426",
                        pointHighlightFill: "#D0A426",
                        pointHighlightStroke: "#D0A426",
                        data: generateDataRM()
                    },
                    {
                        label: "variable",
                        fillColor: "rgba(182,0,184,0)",
                        strokeColor: "#6C354E",
                        pointColor: "#6C354E",
                        pointStrokeColor: "#6C354E",
                        pointHighlightFill: "#6C354E",
                        pointHighlightStroke: "#6C354E",
                        data: generateDataRV()
                    }
                ]
            };
            var grafica = new Chart(ctx).Line(data, {
                showTooltips: true,
                animation: true,
                responsive: true
            });
            grafica.clear();
        }
        $("#chartLine").remove();
        $(".capaCanvasInv").append("<canvas id='chartLine'></canvas>");

        cargarGrafica();

    });


    $scope.getCantidadDisponible = function(){
       dataService.getCantidadDisponible($rootScope.idEmpresaGeneral, $rootScope.idMundoGeneral, $rootScope.momentoGeneral).then(function (result){
            $scope.cantidadDisponible = result.data.cantidad;
			$scope.cantidadDisponibleFormateado = result.data.cantidadFormateado;
            $rootScope.showOverlay = false;

        });
    };



    $scope.guardarFI = function(fondoSeleccionado, comp, vend, operVenta){
        fondoSeleccionado.Comprar = comp;
        fondoSeleccionado.Vender = vend;
		$scope.errorIfSaldoBancario = "";
		$scope.errorIfImporteAlto = "";
		//console.log("guardar FI disponible " + $scope.cantidadDisponible);
		//console.log("guardar FI Comprar " + fondoSeleccionado.Comprar);
		//console.log("guardar FI Vender " + fondoSeleccionado.Vender);
		//console.log("guardar FI operVenta " + operVenta);
		bdatosCorrectos = true;
        //comprar
        if (operVenta == false){
            if ($scope.cantidadDisponible < fondoSeleccionado.Comprar){
                if ($scope.cantidadDisponible < fondoSeleccionado.Comprar)
                {
                    $scope.errorIfSaldoBancario = $translate.instant('Ups.errorIfSaldoBancario');

                }
                $rootScope.mostrarUps = true;
                bdatosCorrectos = false;
            }
			else if (fondoSeleccionado.Comprar < 0)
			{
				$scope.errorIfSaldoBancario = $translate.instant('Ups.errorIfCantidadNegativa');
				$rootScope.mostrarUps = true;
                bdatosCorrectos = false;
				fondoSeleccionado.Comprar  = 0;
			}
        }//vender
        else if(operVenta == true){
            if (fondoSeleccionado.ImporteActual < fondoSeleccionado.Vender){

                $scope.errorIfImporteAlto = $translate.instant('Ups.errorIfImporteAlto');

                $rootScope.mostrarUps = true;
                bdatosCorrectos = false;
            }
			else if (fondoSeleccionado.Vender < 0)
			{
				$scope.errorIfSaldoBancario = $translate.instant('Ups.errorIfCantidadNegativa');
				$rootScope.mostrarUps = true;
                bdatosCorrectos = false;
				fondoSeleccionado.Vender  = 0;
			}
        }

		if (bdatosCorrectos)
		{
            delete fondoSeleccionado["$$hashKey"];
			delete fondoSeleccionado["NombreRentabilidad"];
			fondoSeleccionado = JSON.stringify(fondoSeleccionado);
			//console.log(fondoSeleccionado);
			dataService.guardarFI($rootScope.idEmpresaGeneral, $rootScope.idMundoGeneral, $rootScope.momentoGeneral, fondoSeleccionado).then(function (result){

				$scope.getCantidadDisponible();
                swal($translate.instant('Alertas.operacionCorrecta'));
			});
		}
    };

    $scope.getCantidadDisponible();
}]);
