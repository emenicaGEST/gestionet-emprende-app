emprendeapp.controller('recuperarPassword', ['$scope','dataService','$location','$rootScope', '$translate', function ($scope,dataService,$location,$rootScope,$translate) {
    
	
	$scope.comprobar_pwd_code = function () {
		dataService.comprobar_reset_pwd_code($scope.email, $scope.codigo).then(function (result){
			if(result.data.status=="success"){
				dataService.update_password($scope.email, $scope.password).then(function (result){
					 if(result.data.status=="success"){
							swal({
								title: "",
								text: $translate.instant('Login.actualizarPwd'),
								type: "warning",
								showCancelButton: false,
								confirmButtonColor: "#889641",
								confirmButtonText: $translate.instant('comunes.aceptar'),
								cancelButtonText: $translate.instant('Alertas.no'),
								closeOnConfirm: true
							},
							function(isConfirm)
							{
								if (isConfirm)
								{
								  $location.path("/login");
								   $scope.$apply();
								}
							});
					 }
				});
			}
			if(result.data.status=="fail"){
				swal({
					title: "",
					text: $translate.instant('Login.comprobarRecuperarPwd'),
					type: "warning",
					showCancelButton: false,
					confirmButtonColor: "#889641",
					confirmButtonText: $translate.instant('comunes.aceptar'),
					cancelButtonText: $translate.instant('Alertas.no'),
					closeOnConfirm: true
				});
			}
		   
		});
	};
	
}]);