emprendeapp.directive('modelViewFormat', function () {
   return {
      require: 'ngModel',
	  restrict: 'A',
	  controller: ['$scope', 'customCurrency', function($scope, customCurrency) {
        //pdte revisar
		//http://stackoverflow.com/questions/25560248/angularjs-use-custom-filter-inside-directive-controller
	  }],
      link: function(scope, element, attrs, ngModelController) {
        ngModelController.$parsers.push(function(data) {
          //convert data from view format to model format		  
          return customCurrency(data, "", ".", "", 2); //converted
        });
    
        ngModelController.$formatters.push(function(data) {
          //convert data from model format to view format
          return customCurrency(data, "", ",", ".", 0); //converted
        });
      }
    };
});